import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("UPMDCMessages")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_UsagePointInformation
    colIndex += 1
    Exp_UsagePointInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointInformation: ", Exp_UsagePointInformation)

    global Exp_MDCMessages
    colIndex += 1
    Exp_MDCMessages = sheet.row_values(rowIndex)[colIndex]
    print("Exp_MDCMessages: ", Exp_MDCMessages)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global Meter_col
    colIndex += 1
    Meter_col = sheet.row_values(rowIndex)[colIndex]
    print("Meter_col: ", Meter_col)

    global Meter
    colIndex += 1
    Meter = sheet.row_values(rowIndex)[colIndex]
    print("Meter: ", Meter)

    global UsagePoint_col
    colIndex += 1
    UsagePoint_col = sheet.row_values(rowIndex)[colIndex]
    print("UsagePoint_col: ", UsagePoint_col)

    global UsagePoint
    colIndex += 1
    UsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("UsagePoint: ", UsagePoint)

    global RequestReceived_col
    colIndex += 1
    RequestReceived_col = sheet.row_values(rowIndex)[colIndex]
    print("RequestReceived_col: ", RequestReceived_col)

    global RequestReceived
    colIndex += 1
    RequestReceived = sheet.row_values(rowIndex)[colIndex]
    print("RequestReceived: ", RequestReceived)

    global RequestType_col
    colIndex += 1
    RequestType_col = sheet.row_values(rowIndex)[colIndex]
    print("RequestType_col: ", RequestType_col)

    global RequestType
    colIndex += 1
    RequestType = sheet.row_values(rowIndex)[colIndex]
    print("RequestType: ", RequestType)

    global Override_col
    colIndex += 1
    Override_col = sheet.row_values(rowIndex)[colIndex]
    print("Override_col: ", Override_col)

    global Override
    colIndex += 1
    Override = sheet.row_values(rowIndex)[colIndex]
    print("Override: ", Override)

    global ControlType_col
    colIndex += 1
    ControlType_col = sheet.row_values(rowIndex)[colIndex]
    print("ControlType_col: ", ControlType_col)

    global ControlType
    colIndex += 1
    ControlType = sheet.row_values(rowIndex)[colIndex]
    print("ControlType: ", ControlType)

    global Params_col
    colIndex += 1
    Params_col = sheet.row_values(rowIndex)[colIndex]
    print("Params_col: ", Params_col)

    global Params
    colIndex += 1
    Params = sheet.row_values(rowIndex)[colIndex]
    print("Params: ", Params)

    global Repeats_col
    colIndex += 1
    Repeats_col = sheet.row_values(rowIndex)[colIndex]
    print("Repeats_col: ", Repeats_col)

    global Repeats
    colIndex += 1
    Repeats = sheet.row_values(rowIndex)[colIndex]
    print("Repeats: ", Repeats)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ", Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global TimeCompleted_col
    colIndex += 1
    TimeCompleted_col = sheet.row_values(rowIndex)[colIndex]
    print("TimeCompleted_col: ", TimeCompleted_col)

    global TimeCompleted
    colIndex += 1
    TimeCompleted = sheet.row_values(rowIndex)[colIndex]
    print("TimeCompleted: ", TimeCompleted)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)

def Framework():
    #variables
    UsagePointInfoDiv = 3  # Usage Point information div
    MDCMessagesRow = 4  # MDC Messages row within Usage Point information
    MDCMessagesTableRow = 1  # MDC Messages row within MDC Message row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_UsagePointInformation != "":
        print("Do Something with Exp_UsagePointInformation: ", Exp_UsagePointInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Meter Information')]", Exp_UsagePointInformation)

    if Exp_MDCMessages != "":
        print("Do Something with Exp_MDCMessages: ", Exp_MDCMessages)
        ElementToBeClickableByXPATH_Click("//div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/div[3]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/a[1]/table[1]/tbody[1]/tr[1]/td[2]", Exp_MDCMessages)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[3]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]/div[1]/div[2]", SortOldToRecent)

    if Meter != "":
        print("Do Something with Meter: ", Meter)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, Meter_col, Meter)

    if UsagePoint != "":
        print("Do Something with UsagePoint: ", UsagePoint)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, UsagePoint_col, UsagePoint)

    if RequestReceived != "":
        print("Do Something with RequestReceived: ", RequestReceived)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, RequestReceived_col, RequestReceived)

    if RequestType != "":
        print("Do Something with RequestType: ", RequestType)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, RequestType_col,RequestType)

    if Override != "":
        print("Do Something with Override: ", Override)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, Override_col, Override)

    if ControlType != "":
        print("Do Something with ControlType: ", ControlType)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, ControlType_col,ControlType)

    if Params != "":
        print("Do Something with Params: ", Params)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, Params_col, Params)

    if Repeats != "":
        print("Do Something with Repeats: ", Repeats)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, Repeats_col,Repeats)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, Status_col, Status)

    if TimeCompleted != "":
        print("Do Something with TimeCompleted: ", TimeCompleted)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, MDCMessagesRow, MDCMessagesTableRow, Tr, MoreXpath, RowNumber, TimeCompleted_col, TimeCompleted)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click("//div[3]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[3]/div[1]/div[2]", NextPage)


print("-------- MMA USAGE POINT MDC MESSAGES EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA USAGE POINT MDC MESSAGES EXECUTION COMPLETE --------")

