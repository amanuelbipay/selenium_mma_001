import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("CustomerHist-CustomerHistory")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_CustomerInformation
    colIndex += 1
    Exp_CustomerInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_CustomerInformation: ", Exp_CustomerInformation)

    global Exp_CustomerHistory
    colIndex += 1
    Exp_CustomerHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_CustomerHistory: ", Exp_CustomerHistory)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateModified_col
    colIndex += 1
    DateModified_col = sheet.row_values(rowIndex)[colIndex]
    print("DateModified_col: ", DateModified_col)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global ByUser_col
    colIndex += 1
    ByUser_col = sheet.row_values(rowIndex)[colIndex]
    print("ByUser_col: ", ByUser_col)

    global ByUser
    colIndex += 1
    ByUser = sheet.row_values(rowIndex)[colIndex]
    print("ByUser: ", ByUser)

    global Action_col
    colIndex += 1
    Action_col = sheet.row_values(rowIndex)[colIndex]
    print("Action_col: ", Action_col)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ", Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global Title_col
    colIndex += 1
    Title_col = sheet.row_values(rowIndex)[colIndex]
    print("Title_col: ", Title_col)

    global Title
    colIndex += 1
    Title = sheet.row_values(rowIndex)[colIndex]
    print("Title: ", Title)

    global Initials_col
    colIndex += 1
    Initials_col = sheet.row_values(rowIndex)[colIndex]
    print("Initials_col: ", Initials_col)

    global Initials
    colIndex += 1
    Initials = sheet.row_values(rowIndex)[colIndex]
    print("Initials: ",  Initials)

    global FirstNames_col
    colIndex += 1
    FirstNames_col = sheet.row_values(rowIndex)[colIndex]
    print("FirstNames_col: ", FirstNames_col)

    global FirstNames
    colIndex += 1
    FirstNames = sheet.row_values(rowIndex)[colIndex]
    print("FirstNames: ", FirstNames)

    global Surname_col
    colIndex += 1
    Surname_col = sheet.row_values(rowIndex)[colIndex]
    print("Surname_col: ", Surname_col)

    global Surname
    colIndex += 1
    Surname = sheet.row_values(rowIndex)[colIndex]
    print("Surname: ", Surname)

    global IDNumber_col
    colIndex += 1
    IDNumber_col = sheet.row_values(rowIndex)[colIndex]
    print("IDNumber_col: ", IDNumber_col)

    global IDNumber
    colIndex += 1
    IDNumber = sheet.row_values(rowIndex)[colIndex]
    print("IDNumber: ", IDNumber)

    global Company_col
    colIndex += 1
    Company_col = sheet.row_values(rowIndex)[colIndex]
    print("Company_col: ", Company_col)

    global Company
    colIndex += 1
    Company = sheet.row_values(rowIndex)[colIndex]
    print("Company: ", Company)

    global Email_col
    colIndex += 1
    Email_col = sheet.row_values(rowIndex)[colIndex]
    print("Email_col: ", Email_col)

    global Email
    colIndex += 1
    Email = sheet.row_values(rowIndex)[colIndex]
    print("Email: ", Email)

    global Email2_col
    colIndex += 1
    Email2_col = sheet.row_values(rowIndex)[colIndex]
    print("Email2_col: ", Email2_col)

    global Email2
    colIndex += 1
    Email2 = sheet.row_values(rowIndex)[colIndex]
    print("Email2: ", Email2)

    global Phone_col
    colIndex += 1
    Phone_col = sheet.row_values(rowIndex)[colIndex]
    print("Phone_col: ", Phone_col)

    global Phone
    colIndex += 1
    Phone = sheet.row_values(rowIndex)[colIndex]
    print("Phone: ", Phone)

    global Phone2_col
    colIndex += 1
    Phone2_col = sheet.row_values(rowIndex)[colIndex]
    print("Phone2_col: ", Phone2_col)

    global Phone2
    colIndex += 1
    Phone2 = sheet.row_values(rowIndex)[colIndex]
    print("Phone2: ", Phone2)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)


def Framework():
    #variables
    CustomerInfoDiv = 2  # Customer information div
    CustomerHistoryRow = 4 # Customer history row within Customer information
    CustomerHisTableRow = 1 # Customer history table row within Customer history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_CustomerInformation != "":
        print("Do Something with Exp_CustomerInformation: ", Exp_CustomerInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Customer Information')]", Exp_CustomerInformation)

    if Exp_CustomerHistory != "":
        print("Do Something with Exp_CustomerHistory: ", Exp_CustomerHistory)
        ElementToBeClickableByXPATH_Click("//td[contains(text(),'Customer History')]", Exp_CustomerHistory)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]/div[1]/div[2]", SortOldToRecent)

    if DateModified != "":
        print("Do Something with DateModified: ", DateModified)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, DateModified_col, DateModified)

    if ByUser != "":
        print("Do Something with ByUser: ", ByUser)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, ByUser_col, ByUser)

    if Action != "":
        print("Do Something with Action: ", Action)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Action_col, Action)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Status_col, Status)

    if Title != "":
        print("Do Something with Title: ", Title)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Title_col, Title)

    if Initials != "":
        print("Do Something with Initials: ", Initials)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Initials_col, Initials)

    if FirstNames != "":
        print("Do Something with FirstNames: ", FirstNames)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, FirstNames_col, FirstNames)

    if Surname != "":
        print("Do Something with Surname: ", Surname)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Surname_col, Surname)

    if IDNumber != "":
        print("Do Something with IDNumber: ", IDNumber)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, IDNumber_col, IDNumber)

    if Company != "":
        print("Do Something with Company: ", Company)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Company_col, Company)

    if Email != "":
        print("Do Something with Email: ", Email)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Email_col, Email)

    if Email2 != "":
        print("Do Something with Email2: ", Email2)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Email2_col, Email2)

    if Phone != "":
        print("Do Something with Phone: ", Phone)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Phone_col, Phone)

    if Phone2 != "":
        print("Do Something with Phone2: ", Phone2)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerHisTableRow, Tr, MoreXpath, RowNumber, Phone2_col, Phone2)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]", NextPage)



print("-------- MMA CUSTOMER HISTORY-CUSTOMER HISTORY EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA CUSTOMER HISTORY-CUSTOMER HISTORY EXECUTION COMPLETE --------")



