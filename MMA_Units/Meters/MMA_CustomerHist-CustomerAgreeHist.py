import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("CustomerHist-CustomerAgreeHist")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_CustomerInformation
    colIndex += 1
    Exp_CustomerInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_CustomerInformation: ", Exp_CustomerInformation)

    global Exp_CustomerHistory
    colIndex += 1
    Exp_CustomerHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_CustomerHistory: ", Exp_CustomerHistory)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateModified_col
    colIndex += 1
    DateModified_col = sheet.row_values(rowIndex)[colIndex]
    print("DateModified_col: ", DateModified_col)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global ByUser_col
    colIndex += 1
    ByUser_col = sheet.row_values(rowIndex)[colIndex]
    print("ByUser_col: ", ByUser_col)

    global ByUser
    colIndex += 1
    ByUser = sheet.row_values(rowIndex)[colIndex]
    print("ByUser: ", ByUser)

    global Action_col
    colIndex += 1
    Action_col = sheet.row_values(rowIndex)[colIndex]
    print("Action_col: ", Action_col)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ", Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global Customer_col
    colIndex += 1
    Customer_col = sheet.row_values(rowIndex)[colIndex]
    print("Customer_col: ", Customer_col)

    global Customer
    colIndex += 1
    Customer = sheet.row_values(rowIndex)[colIndex]
    print("Customer: ", Customer)

    global AgreementRef_col
    colIndex += 1
    AgreementRef_col = sheet.row_values(rowIndex)[colIndex]
    print("AgreementRef_col: ", AgreementRef_col)

    global AgreementRef
    colIndex += 1
    AgreementRef = sheet.row_values(rowIndex)[colIndex]
    print("AgreementRef: ", AgreementRef)

    global StartDate_col
    colIndex += 1
    StartDate_col = sheet.row_values(rowIndex)[colIndex]
    print("StartDate_col: ", StartDate_col)

    global StartDate
    colIndex += 1
    StartDate = sheet.row_values(rowIndex)[colIndex]
    print("StartDate: ", StartDate)

    global FreeAuxAccount_col
    colIndex += 1
    FreeAuxAccount_col = sheet.row_values(rowIndex)[colIndex]
    print("FreeAuxAccount_col: ", FreeAuxAccount_col)

    global FreeAuxAccount
    colIndex += 1
    FreeAuxAccount = sheet.row_values(rowIndex)[colIndex]
    print("FreeAuxAccount: ", FreeAuxAccount)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)


def Framework():
    #variables
    CustomerInfoDiv = 2  # Customer information div
    CustomerHistoryRow = 4 # Customer history row within Customer information
    CustomerAgreeTableRow = 2 # Customer agreement history table row within Customer history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_CustomerInformation != "":
        print("Do Something with Exp_CustomerInformation: ", Exp_CustomerInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Customer Information')]", Exp_CustomerInformation)

    if Exp_CustomerHistory != "":
        print("Do Something with Exp_CustomerHistory: ", Exp_CustomerHistory)
        ElementToBeClickableByXPATH_Click("//td[contains(text(),'Customer History')]", Exp_CustomerHistory)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]/div[1]/div[2]", SortOldToRecent)

    if DateModified != "":
        print("Do Something with DateModified: ", DateModified)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, DateModified_col, DateModified)

    if ByUser != "":
        print("Do Something with ByUser: ", ByUser)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, ByUser_col, ByUser)

    if Action != "":
        print("Do Something with Action: ", Action)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, Action_col, Action)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, Status_col, Status)

    if Customer != "":
        print("Do Something with Customer: ", Customer)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, Customer_col, Customer)

    if AgreementRef != "":
        print("Do Something with AgreementRef: ", AgreementRef)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, AgreementRef_col, AgreementRef)

    if StartDate != "":
        print("Do Something with StartDate: ", StartDate)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, StartDate_col, StartDate)

    if FreeAuxAccount != "":
        print("Do Something with FreeAuxAccount: ", FreeAuxAccount)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, CustomerHistoryRow, CustomerAgreeTableRow, Tr, MoreXpath, RowNumber, FreeAuxAccount_col, FreeAuxAccount)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click(
            "//div[2]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]",
            NextPage)



print("-------- MMA CUSTOMER HISTORY-CUSTOMER AGREEEMENT HISTORY EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA CUSTOMER HISTORY-CUSTOMER AGREEEMENT HISTORY EXECUTION COMPLETE --------")



