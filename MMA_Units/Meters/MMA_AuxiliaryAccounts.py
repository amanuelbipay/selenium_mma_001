import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("AuxiliaryAccounts")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_CustomerInformation
    colIndex += 1
    Exp_CustomerInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_CustomerInformation: ", Exp_CustomerInformation)

    global Exp_AuxiliaryAccounts
    colIndex += 1
    Exp_AuxiliaryAccounts = sheet.row_values(rowIndex)[colIndex]
    print("Exp_AuxiliaryAccounts: ", Exp_AuxiliaryAccounts)

    # Start of the Auxiliary Account table
    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global Type_col
    colIndex += 1
    Type_col = sheet.row_values(rowIndex)[colIndex]
    print("Type_col: ", Type_col)

    global Type
    colIndex += 1
    Type = sheet.row_values(rowIndex)[colIndex]
    print("Type: ", Type)

    global AccountName_col
    colIndex += 1
    AccountName_col = sheet.row_values(rowIndex)[colIndex]
    print("AccountName_col: ", AccountName_col)

    global AccountName
    colIndex += 1
    AccountName = sheet.row_values(rowIndex)[colIndex]
    print("AccountName: ", AccountName)

    global StartDate_col
    colIndex += 1
    StartDate_col = sheet.row_values(rowIndex)[colIndex]
    print("StartDate_col: ", StartDate_col)

    global StartDate
    colIndex += 1
    StartDate = sheet.row_values(rowIndex)[colIndex]
    print("StartDate: ", StartDate)

    global SuspendedUntil_col
    colIndex += 1
    SuspendedUntil_col = sheet.row_values(rowIndex)[colIndex]
    print("SuspendedUntil_col: ", SuspendedUntil_col)

    global SuspendedUntil
    colIndex += 1
    SuspendedUntil = sheet.row_values(rowIndex)[colIndex]
    print("SuspendedUntil: ", SuspendedUntil)

    global Balance_col
    colIndex += 1
    Balance_col = sheet.row_values(rowIndex)[colIndex]
    print("Balance_col: ", Balance_col)

    global Balance
    colIndex += 1
    Balance = sheet.row_values(rowIndex)[colIndex]
    print("Balance: ", Balance)

    global Priority_col
    colIndex += 1
    Priority_col = sheet.row_values(rowIndex)[colIndex]
    print("Priority_col: ", Priority_col)

    global Priority
    colIndex += 1
    Priority = sheet.row_values(rowIndex)[colIndex]
    print("Priority: ", Priority)

    global ChargeSchedule_col
    colIndex += 1
    ChargeSchedule_col = sheet.row_values(rowIndex)[colIndex]
    print("ChargeSchedule_col: ", ChargeSchedule_col)

    global ChargeSchedule
    colIndex += 1
    ChargeSchedule = sheet.row_values(rowIndex)[colIndex]
    print("ChargeSchedule: ", ChargeSchedule)

    global FreeIssue_col
    colIndex += 1
    FreeIssue_col = sheet.row_values(rowIndex)[colIndex]
    print("FreeIssue_col: ", FreeIssue_col)

    global FreeIssue
    colIndex += 1
    FreeIssue = sheet.row_values(rowIndex)[colIndex]
    print("FreeIssue: ", FreeIssue)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ", Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)

	# End of the Auxiliary Account table

    #Start of Add/update Auxiliary Account
    global Clk_AccountToUpdate_row
    colIndex += 1
    Clk_AccountToUpdate_row = sheet.row_values(rowIndex)[colIndex]
    print("Clk_AccountToUpdate_row: ", Clk_AccountToUpdate_row)

    global Clk_AccountToUpdate_col
    colIndex += 1
    Clk_AccountToUpdate_col = sheet.row_values(rowIndex)[colIndex]
    print("Clk_AccountToUpdate_col: ", Clk_AccountToUpdate_col)

    global Clk_AccountToUpdate
    colIndex += 1
    Clk_AccountToUpdate = sheet.row_values(rowIndex)[colIndex]
    print("Clk_AccountToUpdate: ", Clk_AccountToUpdate)

    global Drp_AccountType
    colIndex += 1
    Drp_AccountType = sheet.row_values(rowIndex)[colIndex]
    print("Drp_AccountType: ", Drp_AccountType)

    global Fld_AccountName
    colIndex += 1
    Fld_AccountName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AccountName: ", Fld_AccountName)

    global Fld_Balance
    colIndex += 1
    Fld_Balance = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Balance: ", Fld_Balance)

    global Fld_Priority
    colIndex += 1
    Fld_Priority = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Priority: ", Fld_Priority)

    global Fld_SuspendUntil
    colIndex += 1
    Fld_SuspendUntil = sheet.row_values(rowIndex)[colIndex]
    print("Fld_SuspendUntil: ", Fld_SuspendUntil)

    global Fld_StartDate
    colIndex += 1
    Fld_StartDate = sheet.row_values(rowIndex)[colIndex]
    print("Fld_StartDate: ", Fld_StartDate)

    global Fld_DateLastCharged
    colIndex += 1
    Fld_DateLastCharged = sheet.row_values(rowIndex)[colIndex]
    print("Fld_DateLastCharged: ", Fld_DateLastCharged)

    global RdBtn_FunctionsAsDebt
    colIndex += 1
    RdBtn_FunctionsAsDebt = sheet.row_values(rowIndex)[colIndex]
    print("RdBtn_FunctionsAsDebt: ", RdBtn_FunctionsAsDebt)

    global RdBtn_FunctionsAsRefund
    colIndex += 1
    RdBtn_FunctionsAsRefund = sheet.row_values(rowIndex)[colIndex]
    print("RdBtn_FunctionsAsRefund: ", RdBtn_FunctionsAsRefund)

    global Drp_ChargeSchedule
    colIndex += 1
    Drp_ChargeSchedule = sheet.row_values(rowIndex)[colIndex]
    print("Drp_ChargeSchedule: ", Drp_ChargeSchedule)

    global Fld_ReasonForAction
    colIndex += 1
    Fld_ReasonForAction = sheet.row_values(rowIndex)[colIndex]
    print("Fld_ReasonForAction: ", Fld_ReasonForAction)

    global Chk_Active
    colIndex += 1
    Chk_Active = sheet.row_values(rowIndex)[colIndex]
    print("Chk_Active: ", Chk_Active)

    global Btn_Create
    colIndex += 1
    Btn_Create = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Create: ", Btn_Create)

    global Btn_Update
    colIndex += 1
    Btn_Update = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Update: ", Btn_Update)

    global Btn_Cancel
    colIndex += 1
    Btn_Cancel = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Cancel: ", Btn_Cancel)

    global Function
    colIndex += 1
    Function = sheet.row_values(rowIndex)[colIndex]
    print("Function: ",Function)

    global Element
    colIndex += 1
    Element = sheet.row_values(rowIndex)[colIndex]
    print("Element: ", Element)

    global Assert
    colIndex += 1
    Assert = sheet.row_values(rowIndex)[colIndex]
    print("Assert: ", Assert)

    global Btn_Yes
    colIndex += 1
    Btn_Yes = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Yes: ", Btn_Yes)

    global Btn_No
    colIndex += 1
    Btn_No = sheet.row_values(rowIndex)[colIndex]
    print("Btn_No: ", Btn_No)

    global Btn_Close
    colIndex += 1
    Btn_Close = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Close: ", Btn_Close)

    global Btn_DiscardChanges
    colIndex += 1
    Btn_DiscardChanges = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChanges: ", Btn_DiscardChanges)

    global Btn_CancelDiscardChanges
    colIndex += 1
    Btn_CancelDiscardChanges = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelDiscardChanges: ", Btn_CancelDiscardChanges)

    # End of Add/update Auxiliary Account

def Framework():
    #variables
    CustomerInfoDiv = 2  # Customer information div
    AuxiliaryAccRow = 1 # Customer history row within Customer information
    AuxiliaryAccTableRow = 1 # Auxiliary Accounts table row within Customer history row
    Tr = 5
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_CustomerInformation != "":
        print("Do Something with Exp_CustomerInformation: ", Exp_CustomerInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Customer Information')]", Exp_CustomerInformation)

    if Exp_AuxiliaryAccounts != "":
        print("Do Something with Exp_AuxiliaryAccounts: ", Exp_AuxiliaryAccounts)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/a[1]/table[1]/tbody[1]/tr[1]/td[2]", Exp_AuxiliaryAccounts)

    #Start of Table
    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/thead[1]/tr[1]/th[3]",
                                          SortOldToRecent)

    if Type != "":
        print("Do Something with Type: ", Type)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Type_col, Type)

    if AccountName != "":
        print("Do Something with AccountName: ", AccountName)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, AccountName_col, AccountName)

    if StartDate != "":
        print("Do Something with StartDate: ", StartDate)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, StartDate_col, StartDate)

    if SuspendedUntil != "":
        print("Do Something with SuspendedUntil: ", SuspendedUntil)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, SuspendedUntil_col, SuspendedUntil)

    if Balance != "":
        print("Do Something with Balance: ", Balance)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Balance_col, Balance)

    if Priority != "":
        print("Do Something with Priority: ", Priority)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Priority_col, Priority)

    if ChargeSchedule != "":
        print("Do Something with ChargeSchedule: ", ChargeSchedule)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, ChargeSchedule_col, ChargeSchedule)

    if FreeIssue != "":
        print("Do Something with FreeIssue: ", FreeIssue)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, FreeIssue_col, FreeIssue)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Status_col, Status)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click(
            "//div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[6]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]",
            NextPage)
    # End of Table

    # Start of Add/Update Auxiliary Account
    if Clk_AccountToUpdate != "":
        print("Do Something with Clk_AccountToUpdate: ", Clk_AccountToUpdate)
        WebTableElementToBeClickableByXPATH_Click(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, Clk_AccountToUpdate_row, Clk_AccountToUpdate_col, Clk_AccountToUpdate)

    if Drp_AccountType != "":
        print("Do Something with Drp_AccountType: ", Drp_AccountType)
        ElementToBeClickableByID_DropDown("lstbxAuxType", Drp_AccountType)

    if Fld_AccountName != "":
        print("Do Something with Fld_AccountName: ", Fld_AccountName)
        ElementToBeClickableByID_SendKeys("txtbxName", Fld_AccountName)

    if Fld_Balance != "":
        print("Do Something with Fld_Balance: ", Fld_Balance)
        ElementToBeClickableByID_SendKeys("txtbxBalance", Fld_Balance)

    if Fld_Priority != "":
        print("Do Something with Fld_Priority: ", Fld_Priority)
        ElementToBeClickableByID_SendKeys("txtbxPriority", Fld_Priority)

    if Fld_SuspendUntil != "":
        print("Do Something with Fld_SuspendUntil: ", Fld_SuspendUntil)
        ElementToBeClickableByID_SendKeys("suspendUntil", Fld_SuspendUntil)

    if Fld_StartDate != "":
        print("Do Something with Fld_StartDate: ", Fld_StartDate)
        ElementToBeClickableByXPATH_SendKeys("//div[@class='.panel']//div[@class='formElementsPanel']//div//div//input[@id='startDateBox']", Fld_StartDate)

    if Fld_DateLastCharged != "":
        print("Do Something with Fld_DateLastCharged: ", Fld_DateLastCharged)
        ElementToBeClickableByXPATH_SendKeys("//tbody/tr/td[@align='left']/div[@id='auxAccountPanel']/div[@class='.panel']/div[@class='formElementsPanel']/div/div[3]/div[1]/div[2]", Fld_DateLastCharged)

    if RdBtn_FunctionsAsDebt != "":
        print("Do Something with RdBtn_FunctionsAsDebt: ", RdBtn_FunctionsAsDebt)
        ElementToBeClickableByXPATH_Check("(//label[normalize-space()='Debt'])[1]", RdBtn_FunctionsAsDebt)

    if RdBtn_FunctionsAsRefund != "":
        print("Do Something with RdBtn_FunctionsAsRefund: ", RdBtn_FunctionsAsRefund)
        ElementToBeClickableByXPATH_Check("(//label[normalize-space()='Refund'])[1]", RdBtn_FunctionsAsRefund)

    if Drp_ChargeSchedule != "":
        print("Do Something with Drp_ChargeSchedule: ", Drp_ChargeSchedule)
        ElementToBeClickableByID_DropDown("lstbxChargeSchedule", Drp_ChargeSchedule)


    if Fld_ReasonForAction != "":
        print("Do Something with Fld_ReasonForAction: ", Fld_ReasonForAction)
        ElementToBeClickableByXPATH_SendKeys("//body[1]/div[3]/div[2]/div[1]/div[5]/div[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[7]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[4]/input[1]",
                                             Fld_ReasonForAction)

    if Chk_Active != "":
        print("Do Something with Chk_Active: ", Chk_Active)
        ElementToBeClickableByID_Check("chckbxActive-input", Chk_Active)

    if Btn_Create != "":
        print("Do Something with Btn_Create: ", Btn_Create)
        ElementToBeClickableByID_Click("auxAccSaveBtn", Btn_Create)

    if Btn_Update != "":
        print("Do Something with Btn_Update: ", Btn_Update)
        ElementToBeClickableByID_Click("auxAccSaveBtn", Btn_Update)

    if Btn_Cancel != "":
        print("Do Something with Btn_Cancel: ", Btn_Cancel)
        ElementToBeClickableByID_Click("auxAccCancelBtn", Btn_Cancel)

    if Function != "":
        print("Do Something with Function: ", Function)

        if Element != "":
            print("Do Something with Element: ", Element)

            if Assert != "":
                print("Do Something with Assert :", Assert)

                globals()[Function](Element, Assert)

    if Btn_Yes != "":
        print("Do Something with Btn_Yes: ", Btn_Yes)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_Yes)

    if Btn_No != "":
        print("Do Something with Btn_No: ", Btn_No)
        ElementToBeClickableByID_Click("questionPanelCancelButton", Btn_No)

    if Btn_Close != "":
        print("Do Something with Btn_Close: ", Btn_Close)
        ElementToBeClickableByID_Click("informationErrorPanelCloseBtn", Btn_Close)

    if Btn_DiscardChanges != "":
        print("Do Something with Btn_DiscardChanges: ", Btn_DiscardChanges)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChanges)

    if Btn_CancelDiscardChanges != "":
        print("Do Something with Btn_CancelDiscardChanges: ", Btn_CancelDiscardChanges)
        ElementToBeClickableByID_Click("questionPanelCancelButton", Btn_CancelDiscardChanges)

    #End of Add/Update Auxiliary Account

print("-------- MMA AUXILIARY ACCOUNT EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA AUXILIARY ACCOUNT EXECUTION COMPLETE --------")
