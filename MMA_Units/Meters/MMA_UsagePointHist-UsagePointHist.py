import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("UsagePointHist-UsagePointHist")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_UsagePointInformation
    colIndex += 1
    Exp_UsagePointInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointInformation: ", Exp_UsagePointInformation)

    global Exp_UsagePointHistory
    colIndex += 1
    Exp_UsagePointHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointHistory: ", Exp_UsagePointHistory)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateModified_col
    colIndex += 1
    DateModified_col = sheet.row_values(rowIndex)[colIndex]
    print("DateModified_col: ", DateModified_col)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global ByUser_col
    colIndex += 1
    ByUser_col = sheet.row_values(rowIndex)[colIndex]
    print("ByUser_col: ", ByUser_col)

    global ByUser
    colIndex += 1
    ByUser = sheet.row_values(rowIndex)[colIndex]
    print("ByUser: ", ByUser)

    global Action_col
    colIndex += 1
    Action_col = sheet.row_values(rowIndex)[colIndex]
    print("Action_col: ", Action_col)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ", Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global StatusReason_col
    colIndex += 1
    StatusReason_col = sheet.row_values(rowIndex)[colIndex]
    print("StatusReason_col: ", StatusReason_col)

    global StatusReason
    colIndex += 1
    StatusReason = sheet.row_values(rowIndex)[colIndex]
    print("StatusReason: ", StatusReason)

    global Name_col
    colIndex += 1
    Name_col = sheet.row_values(rowIndex)[colIndex]
    print("Name_col: ", Name_col)

    global Name
    colIndex += 1
    Name = sheet.row_values(rowIndex)[colIndex]
    print("Name: ", Name)

    global Meter_col
    colIndex += 1
    Meter_col = sheet.row_values(rowIndex)[colIndex]
    print("Meter_col: ", Meter_col)

    global Meter
    colIndex += 1
    Meter = sheet.row_values(rowIndex)[colIndex]
    print("Meter: ", Meter)

    global CustomerAgree_col
    colIndex += 1
    CustomerAgree_col = sheet.row_values(rowIndex)[colIndex]
    print("CustomerAgree_col: ", CustomerAgree_col)

    global CustomerAgree
    colIndex += 1
    CustomerAgree = sheet.row_values(rowIndex)[colIndex]
    print("CustomerAgree: ", CustomerAgree)

    global ServiceLocation_col
    colIndex += 1
    ServiceLocation_col = sheet.row_values(rowIndex)[colIndex]
    print("ServiceLocation_col: ", ServiceLocation_col)

    global ServiceLocation
    colIndex += 1
    ServiceLocation = sheet.row_values(rowIndex)[colIndex]
    print("ServiceLocation: ", ServiceLocation)

    global Pricing_col
    colIndex += 1
    Pricing_col = sheet.row_values(rowIndex)[colIndex]
    print("Pricing_col: ", Pricing_col)

    global Pricing
    colIndex += 1
    Pricing = sheet.row_values(rowIndex)[colIndex]
    print("Pricing: ", Pricing)

    global BlockingType_col
    colIndex += 1
    BlockingType_col = sheet.row_values(rowIndex)[colIndex]
    print("BlockingType_col: ", BlockingType_col)

    global BlockingType
    colIndex += 1
    BlockingType = sheet.row_values(rowIndex)[colIndex]
    print("BlockingType: ", BlockingType)

    global BlockingReason_col
    colIndex += 1
    BlockingReason_col = sheet.row_values(rowIndex)[colIndex]
    print("BlockingReason_col: ", BlockingReason_col)

    global BlockingReason
    colIndex += 1
    BlockingReason = sheet.row_values(rowIndex)[colIndex]
    print("BlockingReason: ", BlockingReason)

    global MeterRemoveReason_col
    colIndex += 1
    MeterRemoveReason_col = sheet.row_values(rowIndex)[colIndex]
    print("MeterRemoveReason_col: ", MeterRemoveReason_col)

    global MeterRemoveReason
    colIndex += 1
    MeterRemoveReason = sheet.row_values(rowIndex)[colIndex]
    print("MeterRemoveReason: ", MeterRemoveReason)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)


def Framework():
    #variables
    UsagePointInfoDiv = 3  # Usage Point information div
    UsagePointHistoryRow = 2 # Usage Point history row within Usage Point information
    UsagePointHistTableRow = 1 # Usage Point history table row within Usage Point history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_UsagePointInformation != "":
        print("Do Something with Exp_UsagePointInformation: ", Exp_UsagePointInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Usage Point Information')]", Exp_UsagePointInformation)

    if Exp_UsagePointHistory != "":
        print("Do Something with Exp_UsagePointHistory: ", Exp_UsagePointHistory)
        ElementToBeClickableByXPATH_Click("//td[contains(text(),'Usage Point History')]", Exp_UsagePointHistory)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]/div[1]/div[2]", SortOldToRecent)

    if DateModified != "":
        print("Do Something with DateModified: ", DateModified)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, DateModified_col, DateModified)

    if ByUser != "":
        print("Do Something with ByUser: ", ByUser)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, ByUser_col, ByUser)

    if Action != "":
        print("Do Something with Action: ", Action)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, Action_col, Action)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, Status_col, Status)

    if StatusReason != "":
        print("Do Something with StatusReason: ", StatusReason)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, StatusReason_col, StatusReason)

    if Name != "":
        print("Do Something with Name: ", Name)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, Name_col, Name)

    if Meter != "":
        print("Do Something with Meter: ", Meter)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, Meter_col, Meter)

    if CustomerAgree != "":
        print("Do Something with CustomerAgree: ", CustomerAgree)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, CustomerAgree_col, CustomerAgree)

    if ServiceLocation != "":
        print("Do Something with ServiceLocation: ", ServiceLocation)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, ServiceLocation_col, ServiceLocation)

    if Pricing != "":
        print("Do Something with Pricing: ", Pricing)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, Pricing_col, Pricing)

    if BlockingType != "":
        print("Do Something with BlockingType: ", BlockingType)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, BlockingType_col, BlockingType)

    if MeterRemoveReason != "":
        print("Do Something with MeterRemoveReason: ", MeterRemoveReason)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UsagePointHistTableRow, Tr, MoreXpath, RowNumber, MeterRemoveReason_col, MeterRemoveReason)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click(
            "//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]",
            NextPage)


print("-------- MMA USAGE POINT HISTORY-USAGE POINT HISTORY EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA USAGE POINT HISTORY-USAGE POINT HISTORY EXECUTION COMPLETE --------")


