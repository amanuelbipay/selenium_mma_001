import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("MeterHist-MeterHistory")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_MeterInformation
    colIndex += 1
    Exp_MeterInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_MeterInformation: ", Exp_MeterInformation)

    global Exp_MeterHistory
    colIndex += 1
    Exp_MeterHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_MeterHistory: ", Exp_MeterHistory)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateModified_col
    colIndex += 1
    DateModified_col = sheet.row_values(rowIndex)[colIndex]
    print("DateModified_col: ", DateModified_col)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global ByUser_col
    colIndex += 1
    ByUser_col = sheet.row_values(rowIndex)[colIndex]
    print("ByUser_col: ", ByUser_col)

    global ByUser
    colIndex += 1
    ByUser = sheet.row_values(rowIndex)[colIndex]
    print("ByUser: ", ByUser)

    global Action_col
    colIndex += 1
    Action_col = sheet.row_values(rowIndex)[colIndex]
    print("Action_col: ", Action_col)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ",Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global Serial_col
    colIndex += 1
    Serial_col = sheet.row_values(rowIndex)[colIndex]
    print("Serial_col: ", Serial_col)

    global Serial
    colIndex += 1
    Serial = sheet.row_values(rowIndex)[colIndex]
    print("Serial: ", Serial)

    global UniqueID_col
    colIndex += 1
    UniqueID_col = sheet.row_values(rowIndex)[colIndex]
    print("UniqueID_col: ", UniqueID_col)

    global UniqueID
    colIndex += 1
    UniqueID = sheet.row_values(rowIndex)[colIndex]
    print("UniqueID: ", UniqueID)

    global ExtrenalUniqueID_col
    colIndex += 1
    ExtrenalUniqueID_col = sheet.row_values(rowIndex)[colIndex]
    print("ExtrenalUniqueID_col: ", ExtrenalUniqueID_col)

    global ExtrenalUniqueID
    colIndex += 1
    ExtrenalUniqueID = sheet.row_values(rowIndex)[colIndex]
    print("ExtrenalUniqueID: ", ExtrenalUniqueID)

    global TT_col
    colIndex += 1
    TT_col = sheet.row_values(rowIndex)[colIndex]
    print("TT_col: ", TT_col)

    global TT
    colIndex += 1
    TT = sheet.row_values(rowIndex)[colIndex]
    print("TT: ", TT)

    global Alg_col
    colIndex += 1
    Alg_col = sheet.row_values(rowIndex)[colIndex]
    print("Alg_col: ", Alg_col)

    global Alg
    colIndex += 1
    Alg = sheet.row_values(rowIndex)[colIndex]
    print("Alg: ", Alg)

    global SG_col
    colIndex += 1
    SG_col = sheet.row_values(rowIndex)[colIndex]
    print("SG_col: ", SG_col)

    global SG
    colIndex += 1
    SG = sheet.row_values(rowIndex)[colIndex]
    print("SG: ", SG)

    global KR_col
    colIndex += 1
    KR_col = sheet.row_values(rowIndex)[colIndex]
    print("KR_col: ", KR_col)

    global KR
    colIndex += 1
    KR = sheet.row_values(rowIndex)[colIndex]
    print("KR: ", KR)

    global TI_col
    colIndex += 1
    TI_col = sheet.row_values(rowIndex)[colIndex]
    print("TI_col: ", TI_col)

    global TI
    colIndex += 1
    TI = sheet.row_values(rowIndex)[colIndex]
    print("TI: ", TI)

    global Store_col
    colIndex += 1
    Store_col = sheet.row_values(rowIndex)[colIndex]
    print("Store_col: ", Store_col)

    global Store
    colIndex += 1
    Store = sheet.row_values(rowIndex)[colIndex]
    print("Store: ", Store)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)




def Framework():
    #variables
    MeterInfoDiv = 1  # Meter information div
    MeterHistoryRow = 3 # Meter history row within Meter information
    MeterHisTableRow = 1 # Meter history table row within meter history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_MeterInformation != "":
        print("Do Something with Exp_MeterInformation: ", Exp_MeterInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Meter Information')]", Exp_MeterInformation)

    if Exp_MeterHistory != "":
        print("Do Something with Exp_MeterHistory: ", Exp_MeterHistory)
        ElementToBeClickableByXPATH_Click("//td[contains(text(),'Meter History')]", Exp_MeterHistory)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]/div[1]/div[2]", SortOldToRecent)

    if DateModified != "":
        print("Do Something with DateModified: ", DateModified)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, DateModified_col, DateModified)

    if ByUser != "":
        print("Do Something with User: ", ByUser)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, ByUser_col, ByUser)

    if Action != "":
        print("Do Something with Action: ", Action)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, Action_col, Action)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, Status_col,Status)

    if Serial != "":
        print("Do Something with Serial: ", Serial)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, Serial_col,Serial)

    if UniqueID != "":
        print("Do Something with UniqueID: ", UniqueID)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, UniqueID_col, UniqueID)

    if ExtrenalUniqueID != "":
        print("Do Something with ExtrenalUniqueID: ", ExtrenalUniqueID)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, ExtrenalUniqueID_col, ExtrenalUniqueID)

    if TT != "":
        print("Do Something with TT: ", TT)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, TT_col,TT)

    if Alg != "":
        print("Do Something with Alg: ", Alg)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, Alg_col, Alg)

    if SG != "":
        print("Do Something with SG: ", SG)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, SG_col, SG)

    if KR != "":
        print("Do Something with KR: ", KR)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, KR_col, KR)

    if TI != "":
        print("Do Something with TI: ", TI_col)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, TI_col, TI)

    if Store != "":
        print("Do Something with Store: ", Store)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, MeterHisTableRow, Tr, MoreXpath, RowNumber, Store_col, Store)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click("//tbody[1]/tr[3]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]", NextPage)



print("-------- MMA METER HISTORY EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA METER HISTORY EXECUTION COMPLETE --------")