import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("UsagePointHist-UPMeterInstall")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_UsagePointInformation
    colIndex += 1
    Exp_UsagePointInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointInformation: ", Exp_UsagePointInformation)

    global Exp_UsagePointHistory
    colIndex += 1
    Exp_UsagePointHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointHistory: ", Exp_UsagePointHistory)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateModified_col
    colIndex += 1
    DateModified_col = sheet.row_values(rowIndex)[colIndex]
    print("DateModified_col: ", DateModified_col)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global User_col
    colIndex += 1
    User_col = sheet.row_values(rowIndex)[colIndex]
    print("User_col: ", User_col)

    global User
    colIndex += 1
    User = sheet.row_values(rowIndex)[colIndex]
    print("User: ", User)

    global Action_col
    colIndex += 1
    Action_col = sheet.row_values(rowIndex)[colIndex]
    print("Action_col: ", Action_col)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global Meter_col
    colIndex += 1
    Meter_col = sheet.row_values(rowIndex)[colIndex]
    print("Meter_col: ", Meter_col)

    global Meter
    colIndex += 1
    Meter = sheet.row_values(rowIndex)[colIndex]
    print("Meter: ", Meter)

    global InstallDate_col
    colIndex += 1
    InstallDate_col = sheet.row_values(rowIndex)[colIndex]
    print("InstallDate_col: ", InstallDate_col)

    global InstallDate
    colIndex += 1
    InstallDate = sheet.row_values(rowIndex)[colIndex]
    print("InstallDate: ", InstallDate)

    global RemoveDate_col
    colIndex += 1
    RemoveDate_col = sheet.row_values(rowIndex)[colIndex]
    print("RemoveDate_col: ", RemoveDate_col)

    global RemoveDate
    colIndex += 1
    RemoveDate = sheet.row_values(rowIndex)[colIndex]
    print("RemoveDate: ", RemoveDate)

    global Latitude_col
    colIndex += 1
    Latitude_col = sheet.row_values(rowIndex)[colIndex]
    print("Latitude_col: ", Latitude_col)

    global Latitude
    colIndex += 1
    Latitude = sheet.row_values(rowIndex)[colIndex]
    print("Latitude: ", Latitude)

    global Longitude_col
    colIndex += 1
    Longitude_col = sheet.row_values(rowIndex)[colIndex]
    print("Longitude_col: ", Longitude_col)

    global Longitude
    colIndex += 1
    Longitude = sheet.row_values(rowIndex)[colIndex]
    print("Longitude: ", Longitude)

    global InstallRef_col
    colIndex += 1
    InstallRef_col = sheet.row_values(rowIndex)[colIndex]
    print("InstallRef_col: ", InstallRef_col)

    global InstallRef
    colIndex += 1
    InstallRef = sheet.row_values(rowIndex)[colIndex]
    print("InstallRef: ", InstallRef)

    global RemoveRef_col
    colIndex += 1
    RemoveRef_col = sheet.row_values(rowIndex)[colIndex]
    print("RemoveRef_col: ", RemoveRef_col)

    global RemoveRef
    colIndex += 1
    RemoveRef = sheet.row_values(rowIndex)[colIndex]
    print("RemoveRef: ", RemoveRef)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)

def Framework():
    #variables
    UsagePointInfoDiv = 3  # Usage Point information div
    UsagePointHistoryRow = 2  # Usage Point history row within Usage Point information
    UPMeterInstallHistTableRow = 3  # Usage Point Meter Installations table row within Usage Point history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_UsagePointInformation != "":
        print("Do Something with Exp_UsagePointInformation: ", Exp_UsagePointInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Usage Point Information')]", Exp_UsagePointInformation)

    if Exp_UsagePointHistory != "":
        print("Do Something with Exp_UsagePointHistory: ", Exp_UsagePointHistory)
        ElementToBeClickableByXPATH_Click("//td[contains(text(),'Usage Point History')]", Exp_UsagePointHistory)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[3]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]", SortOldToRecent)

    if DateModified != "":
        print("Do Something with DateModified: ", DateModified)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, DateModified_col, DateModified)

    if User != "":
        print("Do Something with User: ", User)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, User_col, User)

    if Action != "":
        print("Do Something with Action: ", Action)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, Action_col, Action)

    if Meter != "":
        print("Do Something with Meter: ", Meter)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, Meter_col, Meter)

    if InstallDate != "":
        print("Do Something with InstallDate: ", InstallDate)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, InstallDate_col,InstallDate)

    if RemoveDate != "":
        print("Do Something with RemoveDate: ", RemoveDate)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, RemoveDate_col, RemoveDate)

    if Latitude != "":
        print("Do Something with Latitude: ", Latitude)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, Latitude_col,Latitude)

    if Longitude != "":
        print("Do Something with Longitude: ", Longitude)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, Longitude_col, Longitude)

    if InstallRef != "":
        print("Do Something with InstallRef: ", InstallRef)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, InstallRef_col,InstallRef)

    if RemoveRef != "":
        print("Do Something with RemoveRef: ", RemoveRef)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPMeterInstallHistTableRow, Tr, MoreXpath, RowNumber, RemoveRef_col, RemoveRef)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click("//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[3]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]", NextPage)


print("-------- MMA USAGE POINT HISTORY - USAGE POINT METER INSTALLATIONS EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA USAGE POINT HISTORY - USAGE POINT METER INSTALLATIONS EXECUTION COMPLETE --------")
