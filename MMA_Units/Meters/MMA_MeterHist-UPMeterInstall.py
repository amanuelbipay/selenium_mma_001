import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("MeterHist-UPMeterInstall")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_MeterInformation
    colIndex += 1
    Exp_MeterInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_MeterInformation: ", Exp_MeterInformation)

    global Exp_MeterHistory
    colIndex += 1
    Exp_MeterHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_MeterHistory: ", Exp_MeterHistory)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateModified_col
    colIndex += 1
    DateModified_col = sheet.row_values(rowIndex)[colIndex]
    print("DateModified_col: ", DateModified_col)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global User_col
    colIndex += 1
    User_col = sheet.row_values(rowIndex)[colIndex]
    print("User_col: ", User_col)

    global User
    colIndex += 1
    User = sheet.row_values(rowIndex)[colIndex]
    print("User: ", User)

    global Action_col
    colIndex += 1
    Action_col = sheet.row_values(rowIndex)[colIndex]
    print("Action_col: ", Action_col)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global UsagePoint_col
    colIndex += 1
    UsagePoint_col = sheet.row_values(rowIndex)[colIndex]
    print("UsagePoint_col: ", UsagePoint_col)

    global UsagePoint
    colIndex += 1
    UsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("UsagePoint: ", UsagePoint)

    global InstallDate_col
    colIndex += 1
    InstallDate_col = sheet.row_values(rowIndex)[colIndex]
    print("InstallDate_col: ", InstallDate_col)

    global InstallDate
    colIndex += 1
    InstallDate = sheet.row_values(rowIndex)[colIndex]
    print("InstallDate: ", InstallDate)

    global RemoveDate_col
    colIndex += 1
    RemoveDate_col = sheet.row_values(rowIndex)[colIndex]
    print("RemoveDate_col: ", RemoveDate_col)

    global RemoveDate
    colIndex += 1
    RemoveDate = sheet.row_values(rowIndex)[colIndex]
    print("RemoveDate: ", RemoveDate)

    global Latitude_col
    colIndex += 1
    Latitude_col = sheet.row_values(rowIndex)[colIndex]
    print("Latitude_col: ", Latitude_col)

    global Latitude
    colIndex += 1
    Latitude = sheet.row_values(rowIndex)[colIndex]
    print("Latitude: ", Latitude)

    global Longitude_col
    colIndex += 1
    Longitude_col = sheet.row_values(rowIndex)[colIndex]
    print("Longitude_col: ", Longitude_col)

    global Longitude
    colIndex += 1
    Longitude = sheet.row_values(rowIndex)[colIndex]
    print("Longitude: ", Longitude)

    global InstallRef_col
    colIndex += 1
    InstallRef_col = sheet.row_values(rowIndex)[colIndex]
    print("InstallRef_col: ", InstallRef_col)

    global InstallRef
    colIndex += 1
    InstallRef = sheet.row_values(rowIndex)[colIndex]
    print("InstallRef: ", InstallRef)

    global RemoveRef_col
    colIndex += 1
    RemoveRef_col = sheet.row_values(rowIndex)[colIndex]
    print("RemoveRef_col: ", RemoveRef_col)

    global RemoveRef
    colIndex += 1
    RemoveRef = sheet.row_values(rowIndex)[colIndex]
    print("RemoveRef: ", RemoveRef)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)


def Framework():
    #variables
    MeterInfoDiv = 1  # Meter information div
    MeterHistoryRow = 3 # Meter history row within Meter information
    UPMeterInstallTableRow = 2 # Meter history table row within meter history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_MeterInformation != "":
        print("Do Something with Exp_MeterInformation: ", Exp_MeterInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Meter Information')]", Exp_MeterInformation)

    if Exp_MeterHistory != "":
        print("Do Something with Exp_MeterHistory: ", Exp_MeterHistory)
        ElementToBeClickableByXPATH_Click("//td[contains(text(),'Meter History')]", Exp_MeterHistory)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[1]/table[1]/tbody[1]/tr[3]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]", SortOldToRecent)

    if DateModified != "":
        print("Do Something with DateModified: ", DateModified)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, DateModified_col, DateModified)

    if User != "":
        print("Do Something with User: ", User)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, User_col, User)

    if Action != "":
        print("Do Something with Action: ", Action)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, Action_col, Action)

    if UsagePoint != "":
        print("Do Something with UsagePoint: ", UsagePoint)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, UsagePoint_col, UsagePoint)

    if InstallDate != "":
        print("Do Something with InstallDate: ", InstallDate)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, InstallDate_col,InstallDate)

    if RemoveDate != "":
        print("Do Something with RemoveDate: ", RemoveDate)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, RemoveDate_col, RemoveDate)

    if Latitude != "":
        print("Do Something with Latitude: ", Latitude)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, Latitude_col,Latitude)

    if Longitude != "":
        print("Do Something with Longitude: ", Longitude)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, Longitude_col, Longitude)

    if InstallRef != "":
        print("Do Something with InstallRef: ", InstallRef)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, InstallRef_col,InstallRef)

    if RemoveRef != "":
        print("Do Something with RemoveRef: ", RemoveRef)
        WebTableTextToBePresentInElementByXPATH(MeterInfoDiv, MeterHistoryRow, UPMeterInstallTableRow, Tr, MoreXpath, RowNumber, RemoveRef_col, RemoveRef)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click("//div[1]/table[1]/tbody[1]/tr[3]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]", NextPage)


print("-------- MMA METER HISTORY - USAGE POINT METER INSTALLATIONS EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA METER HISTORY- USAGE POINT METER INSTALLATIONS EXECUTION COMPLETE --------")
