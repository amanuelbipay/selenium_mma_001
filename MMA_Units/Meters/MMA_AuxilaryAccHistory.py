import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("AuxilaryAccHistTable")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_CustomerInformation
    colIndex += 1
    Exp_CustomerInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_CustomerInformation: ", Exp_CustomerInformation)

    global Exp_AuxiliaryAccountsHistory
    colIndex += 1
    Exp_AuxiliaryAccountsHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_AuxiliaryAccountsHistory: ", Exp_AuxiliaryAccountsHistory)

    # Start of the Auxiliary Account History table
    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global Type_col
    colIndex += 1
    Type_col = sheet.row_values(rowIndex)[colIndex]
    print("Type_col: ", Type_col)

    global Type
    colIndex += 1
    Type = sheet.row_values(rowIndex)[colIndex]
    print("Type: ", Type)

    global AccountName_col
    colIndex += 1
    AccountName_col = sheet.row_values(rowIndex)[colIndex]
    print("AccountName_col: ", AccountName_col)

    global AccountName
    colIndex += 1
    AccountName = sheet.row_values(rowIndex)[colIndex]
    print("AccountName: ", AccountName)

    global SuspendedUntil_col
    colIndex += 1
    SuspendedUntil_col = sheet.row_values(rowIndex)[colIndex]
    print("SuspendedUntil_col: ", SuspendedUntil_col)

    global SuspendedUntil
    colIndex += 1
    SuspendedUntil = sheet.row_values(rowIndex)[colIndex]
    print("SuspendedUntil: ", SuspendedUntil)

    global Balance_col
    colIndex += 1
    Balance_col = sheet.row_values(rowIndex)[colIndex]
    print("Balance_col: ", Balance_col)

    global Balance
    colIndex += 1
    Balance = sheet.row_values(rowIndex)[colIndex]
    print("Balance: ", Balance)

    global Priority_col
    colIndex += 1
    Priority_col = sheet.row_values(rowIndex)[colIndex]
    print("Priority_col: ", Priority_col)

    global Priority
    colIndex += 1
    Priority = sheet.row_values(rowIndex)[colIndex]
    print("Priority: ", Priority)

    global ChargeSchedule_col
    colIndex += 1
    ChargeSchedule_col = sheet.row_values(rowIndex)[colIndex]
    print("ChargeSchedule_col: ", ChargeSchedule_col)

    global ChargeSchedule
    colIndex += 1
    ChargeSchedule = sheet.row_values(rowIndex)[colIndex]
    print("ChargeSchedule: ", ChargeSchedule)

    global FreeIssue_col
    colIndex += 1
    FreeIssue_col = sheet.row_values(rowIndex)[colIndex]
    print("FreeIssue_col: ", FreeIssue_col)

    global FreeIssue
    colIndex += 1
    FreeIssue = sheet.row_values(rowIndex)[colIndex]
    print("FreeIssue: ", FreeIssue)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ", Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)

	# End of the Auxiliary Account History table

def Framework():
    #variables
    CustomerInfoDiv = 2  # Customer information div
    AuxiliaryAccRow = 2 # Customer history row within Customer information
    AuxiliaryAccTableRow = 1 # Auxiliary Accounts table row within Customer history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_CustomerInformation != "":
        print("Do Something with Exp_CustomerInformation: ", Exp_CustomerInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Customer Information')]", Exp_CustomerInformation)

    if Exp_AuxiliaryAccountsHistory != "":
        print("Do Something with Exp_AuxiliaryAccountsHistory: ", Exp_AuxiliaryAccountsHistory)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/a[1]/table[1]/tbody[1]/tr[1]/td[2]", Exp_AuxiliaryAccountsHistory)

    #Start of Table
    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[2]",
                                          SortOldToRecent)
    if Type != "":
        print("Do Something with Type: ", Type)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Type_col, Type)

    if AccountName != "":
        print("Do Something with AccountName: ", AccountName)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, AccountName_col, AccountName)

    if SuspendedUntil != "":
        print("Do Something with SuspendedUntil: ", SuspendedUntil)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, SuspendedUntil_col, SuspendedUntil)

    if Balance != "":
        print("Do Something with Balance: ", Balance)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Balance_col, Balance)

    if Priority != "":
        print("Do Something with Priority: ", Priority)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Priority_col, Priority)

    if ChargeSchedule != "":
        print("Do Something with ChargeSchedule: ", ChargeSchedule)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, ChargeSchedule_col, ChargeSchedule)

    if FreeIssue != "":
        print("Do Something with FreeIssue: ", FreeIssue)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, FreeIssue_col, FreeIssue)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxiliaryAccTableRow, Tr, MoreXpath, RowNumber, Status_col, Status)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click(
            "//div[2]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]",
            NextPage)
    # End of Table

print("-------- MMA AUXILIARY ACCOUNT HISTORY EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA AUXILIARY ACCOUNT HISTORY EXECUTION COMPLETE --------")