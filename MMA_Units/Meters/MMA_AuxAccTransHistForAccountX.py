import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("AuxAccTransHistForAccountX")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_CustomerInformation
    colIndex += 1
    Exp_CustomerInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_CustomerInformation: ", Exp_CustomerInformation)

    global Exp_AuxiliaryAccounts
    colIndex += 1
    Exp_AuxiliaryAccounts = sheet.row_values(rowIndex)[colIndex]
    print("Exp_AuxiliaryAccounts: ", Exp_AuxiliaryAccounts)

    global Clk_AccountName_row
    colIndex += 1
    Clk_AccountName_row = sheet.row_values(rowIndex)[colIndex]
    print("Clk_AccountName_row: ", Clk_AccountName_row)

    global Clk_AccountName_col
    colIndex += 1
    Clk_AccountName_col = sheet.row_values(rowIndex)[colIndex]
    print("Clk_AccountName_col: ", Clk_AccountName_col)

    global Clk_AccountName
    colIndex += 1
    Clk_AccountName = sheet.row_values(rowIndex)[colIndex]
    print("Clk_AccountName: ", Clk_AccountName)

    # Start of Auxiliary Transaction History for Account X Table table
    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateEntered_col
    colIndex += 1
    DateEntered_col = sheet.row_values(rowIndex)[colIndex]
    print("DateEntered_col: ", DateEntered_col)

    global DateEntered
    colIndex += 1
    DateEntered = sheet.row_values(rowIndex)[colIndex]
    print("DateEntered: ", DateEntered)

    global UserEntered_col
    colIndex += 1
    UserEntered_col = sheet.row_values(rowIndex)[colIndex]
    print("UserEntered_col: ", UserEntered_col)

    global UserEntered
    colIndex += 1
    UserEntered = sheet.row_values(rowIndex)[colIndex]
    print("UserEntered: ", UserEntered)

    global TransactionType_col
    colIndex += 1
    TransactionType_col = sheet.row_values(rowIndex)[colIndex]
    print("TransactionType_col: ", TransactionType_col)

    global TransactionType
    colIndex += 1
    TransactionType = sheet.row_values(rowIndex)[colIndex]
    print("TransactionType: ", TransactionType)

    global TransactionDate_col
    colIndex += 1
    TransactionDate_col = sheet.row_values(rowIndex)[colIndex]
    print("TransactionDate_col: ", TransactionDate_col)

    global TransactionDate
    colIndex += 1
    TransactionDate = sheet.row_values(rowIndex)[colIndex]
    print("TransactionDate: ", TransactionDate)

    global Comment_col
    colIndex += 1
    Comment_col = sheet.row_values(rowIndex)[colIndex]
    print("Comment_col: ", Comment_col)

    global Comment
    colIndex += 1
    Comment = sheet.row_values(rowIndex)[colIndex]
    print("Comment: ", Comment)

    global ActionReason_col
    colIndex += 1
    ActionReason_col = sheet.row_values(rowIndex)[colIndex]
    print("ActionReason_col: ", ActionReason_col)

    global ActionReason
    colIndex += 1
    ActionReason = sheet.row_values(rowIndex)[colIndex]
    print("ActionReason: ", ActionReason)

    global OurReference_col
    colIndex += 1
    OurReference_col = sheet.row_values(rowIndex)[colIndex]
    print("OurReference_col: ", OurReference_col)

    global OurReference
    colIndex += 1
    OurReference = sheet.row_values(rowIndex)[colIndex]
    print("OurReference: ", OurReference)

    global AmountInclTax_col
    colIndex += 1
    AmountInclTax_col = sheet.row_values(rowIndex)[colIndex]
    print("AmountInclTax_col: ", AmountInclTax_col)

    global AmountInclTax
    colIndex += 1
    AmountInclTax = sheet.row_values(rowIndex)[colIndex]
    print("AmountInclTax: ", AmountInclTax)

    global Tax_col
    colIndex += 1
    Tax_col = sheet.row_values(rowIndex)[colIndex]
    print("Tax_col: ", Tax_col)

    global Tax
    colIndex += 1
    Tax = sheet.row_values(rowIndex)[colIndex]
    print("Tax: ", Tax)

    global Balance_col
    colIndex += 1
    Balance_col = sheet.row_values(rowIndex)[colIndex]
    print("Balance_col: ", Balance_col)

    global Balance
    colIndex += 1
    Balance = sheet.row_values(rowIndex)[colIndex]
    print("Balance: ", Balance)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)
    #End of Auxiliary Transaction History for Account X Table table

    #Start of Adjust Auxiliary Account
    global Fld_AccountRef
    colIndex += 1
    Fld_AccountRef = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AccountRef: ", Fld_AccountRef)

    global Fld_OurReference
    colIndex += 1
    Fld_OurReference = sheet.row_values(rowIndex)[colIndex]
    print("Fld_OurReference: ", Fld_OurReference)

    global RdBtn_IncreaseDebtBy
    colIndex += 1
    RdBtn_IncreaseDebtBy = sheet.row_values(rowIndex)[colIndex]
    print("RdBtn_IncreaseDebtBy: ", RdBtn_IncreaseDebtBy)

    global RdBtn_DecreaseDebtBy
    colIndex += 1
    RdBtn_DecreaseDebtBy = sheet.row_values(rowIndex)[colIndex]
    print("RdBtn_DecreaseDebtBy: ", RdBtn_DecreaseDebtBy)

    global Fld_AmountInclTax
    colIndex += 1
    Fld_AmountInclTax = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AmountInclTax: ", Fld_AmountInclTax)

    global Fld_Tax
    colIndex += 1
    Fld_Tax = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Tax: ", Fld_Tax)

    global Fld_Comment
    colIndex += 1
    Fld_Comment = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Comment: ", Fld_Comment)

    global Fld_ActionReason
    colIndex += 1
    Fld_ActionReason = sheet.row_values(rowIndex)[colIndex]
    print("Fld_ActionReason: ", Fld_ActionReason)

    global Btn_Update
    colIndex += 1
    Btn_Update = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Update: ", Btn_Update)

    global Btn_Cancel
    colIndex += 1
    Btn_Cancel = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Cancel: ", Btn_Cancel)

    global Function
    colIndex += 1
    Function = sheet.row_values(rowIndex)[colIndex]
    print("Function: ", Function)

    global Element
    colIndex += 1
    Element = sheet.row_values(rowIndex)[colIndex]
    print("Element: ", Element)

    global Assert
    colIndex += 1
    Assert = sheet.row_values(rowIndex)[colIndex]
    print("Assert: ", Assert)

    global Btn_Close
    colIndex += 1
    Btn_Close = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Close: ", Btn_Close)

    global Btn_DiscardChanges
    colIndex += 1
    Btn_DiscardChanges = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChanges: ", Btn_DiscardChanges)

    global Btn_CancelDiscardChanges
    colIndex += 1
    Btn_CancelDiscardChanges = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelDiscardChanges: ", Btn_CancelDiscardChanges)
    # End of Adjust Auxiliary Account

def Framework():
    #variables
    CustomerInfoDiv = 2  # Customer information div
    AuxiliaryAccRow = 1 # Auxiliary Accounts row within Customer information
    AuxAccTransTableRow = 1 # Auxiliary Transaction History for Account X Table  within Auxiliary Accounts row
    Tr = 8
    MoreXpath = "div[1]/table[1]/tbody[1]/tr[5]/td[1]/"
    AuxAccTr = 5
    AuxAccMoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_CustomerInformation != "":
        print("Do Something with Exp_CustomerInformation: ", Exp_CustomerInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Customer Information')]", Exp_CustomerInformation)

    if Exp_AuxiliaryAccounts != "":
        print("Do Something with Exp_AuxiliaryAccounts: ", Exp_AuxiliaryAccounts)
        ElementToBeClickableByXPATH_Click("//div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/a[1]/table[1]/tbody[1]/tr[1]/td[2]", Exp_AuxiliaryAccounts)

    if Clk_AccountName != "":
        print("Do Something with Clk_AccountName: ", Clk_AccountName)
        WebTableElementToBeClickableByXPATH_Click(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, AuxAccTr, AuxAccMoreXpath, Clk_AccountName_row, Clk_AccountName_col, Clk_AccountName)

    #Start of Table
    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Check("//div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[8]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/thead[1]/tr[1]/th[1]/div[1]/div[2]",
                                          SortOldToRecent)

    if DateEntered != "":
        print("Do Something with DateEntered: ", DateEntered)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath, RowNumber, DateEntered_col, DateEntered)

    if UserEntered != "":
        print("Do Something with UserEntered: ", UserEntered)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath, RowNumber, UserEntered_col, UserEntered)

    if TransactionType != "":
        print("Do Something with TransactionType: ", TransactionType)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, TransactionType_col, TransactionType)

    if TransactionDate != "":
        print("Do Something with TransactionDate: ", TransactionDate)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, TransactionDate_col, TransactionDate)

    if Comment != "":
        print("Do Something with Comment: ", Comment)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, Comment_col, Comment)

    if ActionReason != "":
        print("Do Something with ActionReason: ", ActionReason)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, ActionReason_col, ActionReason)

    if OurReference != "":
        print("Do Something with OurReference: ", OurReference)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, OurReference_col, OurReference)

    if AmountInclTax != "":
        print("Do Something with AmountInclTax: ", AmountInclTax)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, AmountInclTax_col, AmountInclTax)

    if Tax != "":
        print("Do Something with Tax: ", Tax)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, Tax_col, Tax)

    if Balance != "":
        print("Do Something with Balance: ", Balance)
        WebTableTextToBePresentInElementByXPATH(CustomerInfoDiv, AuxiliaryAccRow, AuxAccTransTableRow, Tr, MoreXpath,
                                                RowNumber, Balance_col, Balance)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click(
            "//div[contains(text(),'Date Entered')]",
            NextPage)

    # End of Table

    # Start of Adjust Auxiliary Account
    if Fld_AccountRef != "":
        print("Do Something with Fld_AccountRef: ", Fld_AccountRef)
        ElementToBeClickableByID_SendKeys("txtbxAccRef", Fld_AccountRef)

    if Fld_OurReference != "":
        print("Do Something with Fld_OurReference: ", Fld_OurReference)
        ElementToBeClickableByID_SendKeys("txtbxOurRef", Fld_OurReference)

    if RdBtn_IncreaseDebtBy != "":
        print("Do Something with RdBtn_IncreaseDebtBy: ", RdBtn_IncreaseDebtBy)
        ElementToBeClickableByXPATH_Check("(//label[normalize-space()='Increase DEBT by:'])[1]", RdBtn_IncreaseDebtBy)

    if RdBtn_DecreaseDebtBy != "":
        print("Do Something with RdBtn_DecreaseDebtBy: ", RdBtn_DecreaseDebtBy)
        ElementToBeClickableByXPATH_Check("(//label[normalize-space()='Decrease DEBT by:'])[1]", RdBtn_DecreaseDebtBy)

    if Fld_AmountInclTax != "":
        print("Do Something with Fld_AmountInclTax: ", Fld_AmountInclTax)
        ElementToBeClickableByID_SendKeys("txtbxAmt", Fld_AmountInclTax)

    if Fld_Tax != "":
        print("Do Something with Fld_Tax: ", Fld_Tax)
        ElementToBeClickableByID_SendKeys("txtbxTax", Fld_Tax)

    if Fld_Comment != "":
        print("Do Something with Fld_Comment: ", Fld_Comment)
        ElementToBeClickableByID_SendKeys("txtbxComment", Fld_Comment)

    if Fld_ActionReason != "":
        print("Do Something with Fld_ActionReason: ", Fld_ActionReason)
        ElementToBeClickableByXPATH_SendKeys("//div[2]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[8]/td[1]/div[1]/table[1]/tbody[1]/tr[7]/td[1]/div[1]/div[1]/div[2]/div[1]/fieldset[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[4]/input[1]",
                                             Fld_ActionReason)

    if Btn_Update != "":
        print("Do Something with Btn_Update: ", Btn_Update)
        ElementToBeClickableByID_Click("saveButton", Btn_Update)

    if Btn_Cancel != "":
        print("Do Something with Btn_Cancel: ", Btn_Cancel)
        ElementToBeClickableByID_Click("cancelButton", Btn_Cancel)

    if Function != "":
        print("Do Something with Function: ", Function)

        if Element != "":
            print("Do Something with Element: ", Element)

            if Assert != "":
                print("Do Something with Assert :", Assert)

                globals()[Function](Element, Assert)

    if Btn_Close != "":
        print("Do Something with Btn_Close: ", Btn_Close)
        ElementToBeClickableByID_Click("informationErrorPanelCloseBtn", Btn_Close)

    if Btn_DiscardChanges != "":
        print("Do Something with Btn_DiscardChanges: ", Btn_DiscardChanges)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChanges)

    if Btn_CancelDiscardChanges != "":
        print("Do Something with Btn_CancelDiscardChanges: ", Btn_CancelDiscardChanges)
        ElementToBeClickableByID_Click("questionPanelCancelButton", Btn_CancelDiscardChanges)


print("-------- MMA AUXILIARY TRANSACTION HISTORY FOR ACCOUNT X EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA AUXILIARY TRANSACTION HISTORY FOR ACCOUNT X EXECUTION COMPLETE --------")