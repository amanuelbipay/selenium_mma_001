#from Functions.Functions import *
"""\
from Functions.Functions import ElementToBeClickableByID_SendKeys, ElementToBeClickableByXPATH_SendKeys, \
    ElementToBeClickableByID_Click, ElementToBeClickableByXPATH_Click, ElementToBeClickableByID_DropDown, \
    ElementToBeClickableByID_Check, ElementToBeClickableByNAME_SendKeys
"""

import xlrd
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import Select

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("AddNewMeterCustomerUsagePoint")


def ReadData(rowIndex):
    colIndex = -1


# Control Flow
    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)


#Search Options
    global MnuMainSearch
    colIndex += 1
    MnuMainSearch = sheet.row_values(rowIndex)[colIndex]
    print("MnuMainSearch: ", MnuMainSearch)

    global Fld_SearchByMeterNumber
    colIndex += 1
    Fld_SearchByMeterNumber = sheet.row_values(rowIndex)[colIndex]
    print("Fld_SearchByMeterNumber: ", Fld_SearchByMeterNumber)

    global Btn_SearchByMeterNumber
    colIndex += 1
    Btn_SearchByMeterNumber = sheet.row_values(rowIndex)[colIndex]
    print("Btn_SearchByMeterNumber: ", Btn_SearchByMeterNumber)

    global Fld_SearchByUsagePointName
    colIndex += 1
    Fld_SearchByUsagePointName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_SearchByUsagePointName: ", Fld_SearchByUsagePointName)

    global Btn_SearchByUsagePointName
    colIndex += 1
    Btn_SearchByUsagePointName = sheet.row_values(rowIndex)[colIndex]
    print("Btn_SearchByUsagePointName: ", Btn_SearchByUsagePointName)

    global Drp_CustomerSearchBy
    colIndex += 1
    Drp_CustomerSearchBy = sheet.row_values(rowIndex)[colIndex]
    print("Drp_CustomerSearchBy: ", Drp_CustomerSearchBy)

    global Fld_CustomerSearchBySurname
    colIndex += 1
    Fld_CustomerSearchBySurname = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CustomerSearchBySurname: ", Fld_CustomerSearchBySurname)

    global Fld_CustomerSearchByAgreementRef
    colIndex += 1
    Fld_CustomerSearchByAgreementRef = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CustomerSearchByAgreementRef: ", Fld_CustomerSearchByAgreementRef)

    global Fld_CustomerSearchByAccountName
    colIndex += 1
    Fld_CustomerSearchByAccountName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CustomerSearchByAccountName: ", Fld_CustomerSearchByAccountName)

    global Fld_CustomerSearchByIDNumber
    colIndex += 1
    Fld_CustomerSearchByIDNumber = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CustomerSearchByIDNumber: ", Fld_CustomerSearchByIDNumber)

    global Btn_CustomerSearchBy
    colIndex += 1
    Btn_CustomerSearchBy = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CustomerSearchBy: ", Btn_CustomerSearchBy)

    global Btn_CloseTab_Search
    colIndex += 1
    Btn_CloseTab_Search = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CloseTab_Search: ", Btn_CloseTab_Search)

# Menu Selection

    global MnuMain_Meter
    colIndex += 1
    MnuMain_Meter = sheet.row_values(rowIndex)[colIndex]
    print("MnuMain_Meter: ", MnuMain_Meter)

    global MnuSub_Meter
    colIndex += 1
    MnuSub_Meter = sheet.row_values(rowIndex)[colIndex]
    print("MnuSub_Meter: ", MnuSub_Meter)

# Add/Edit Meter
    global Drp_SelectMeterModel
    colIndex += 1
    Drp_SelectMeterModel = sheet.row_values(rowIndex)[colIndex]
    print("Drp_SelectMeterModel: ", Drp_SelectMeterModel)

    global Fld_MeterNumber
    colIndex += 1
    Fld_MeterNumber = sheet.row_values(rowIndex)[colIndex]
    print("Fld_MeterNumber: ", Fld_MeterNumber)

    global Fld_SerialNumber
    colIndex += 1
    Fld_SerialNumber = sheet.row_values(rowIndex)[colIndex]
    print("Fld_SerialNumber: ", Fld_SerialNumber)

    global Fld_UniqueID
    colIndex += 1
    Fld_UniqueID = sheet.row_values(rowIndex)[colIndex]
    print("Fld_UniqueID: ", Fld_UniqueID)

    global Chk_ExternalUniqueID
    colIndex += 1
    Chk_ExternalUniqueID = sheet.row_values(rowIndex)[colIndex]
    print("Chk_ExternalUniqueID: ", Chk_ExternalUniqueID)

    global Drp_Algorithmcode
    colIndex += 1
    Drp_Algorithmcode = sheet.row_values(rowIndex)[colIndex]
    print("Drp_Algorithmcode: ", Drp_Algorithmcode)

    global Drp_TokenTechCode
    colIndex += 1
    Drp_TokenTechCode = sheet.row_values(rowIndex)[colIndex]
    print("Drp_TokenTechCode: ", Drp_TokenTechCode)

    global Drp_CurrentSupplyGroupCode
    colIndex += 1
    Drp_CurrentSupplyGroupCode = sheet.row_values(rowIndex)[colIndex]
    print("Drp_CurrentSupplyGroupCode: ", Drp_CurrentSupplyGroupCode)

    global Fld_CurrentTariffIndex
    colIndex += 1
    Fld_CurrentTariffIndex = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CurrentTariffIndex: ", Fld_CurrentTariffIndex)

    global Chk_ThreeKeyChangeTokensRequired
    colIndex += 1
    Chk_ThreeKeyChangeTokensRequired = sheet.row_values(rowIndex)[colIndex]
    print("Chk_ThreeKeyChangeTokensRequired: ", Chk_ThreeKeyChangeTokensRequired)

    global Drp_PowerLimit
    colIndex += 1
    Drp_PowerLimit = sheet.row_values(rowIndex)[colIndex]
    print("Drp_PowerLimit: ", Drp_PowerLimit)

    global Drp_GeneratePowerLimitTokens
    colIndex += 1
    Drp_GeneratePowerLimitTokens = sheet.row_values(rowIndex)[colIndex]
    print("Drp_GeneratePowerLimitTokens: ", Drp_GeneratePowerLimitTokens)

    global Drp_MeterStore
    colIndex += 1
    Drp_MeterStore = sheet.row_values(rowIndex)[colIndex]
    print("Drp_MeterStore: ", Drp_MeterStore)

    global Btn_Save_Meter
    colIndex += 1
    Btn_Save_Meter = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Save_Meter: ", Btn_Save_Meter)

    global Btn_Cancel_Meter
    colIndex += 1
    Btn_Cancel_Meter = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Cancel_Meter: ", Btn_Cancel_Meter)

    global Function_Meter
    colIndex += 1
    Function_Meter = sheet.row_values(rowIndex)[colIndex]
    print("Function_Meter: ", Function_Meter)

    global Element_Meter
    colIndex += 1
    Element_Meter = sheet.row_values(rowIndex)[colIndex]
    print("Element_Meter: ", Element_Meter)

    global Assert_Meter
    colIndex += 1
    Assert_Meter = sheet.row_values(rowIndex)[colIndex]
    print("Assert_Meter: ", Assert_Meter)

    global Btn_DiscardChangesNewMeter
    colIndex += 1
    Btn_DiscardChangesNewMeter = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChangesNewMeter: ", Btn_DiscardChangesNewMeter)

# Fetch Meter
    global Lnk_FetchMeterFromDeviceStore
    colIndex += 1
    Lnk_FetchMeterFromDeviceStore = sheet.row_values(rowIndex)[colIndex]
    print("Lnk_FetchMeterFromDeviceStore: ", Lnk_FetchMeterFromDeviceStore)

    global Fld_EnterMeterNumberFetchMeter
    colIndex += 1
    Fld_EnterMeterNumberFetchMeter = sheet.row_values(rowIndex)[colIndex]
    print("Fld_EnterMeterNumberFetchMeter: ", Fld_EnterMeterNumberFetchMeter)

    global Btn_FetchMeter
    colIndex += 1
    Btn_FetchMeter = sheet.row_values(rowIndex)[colIndex]
    print("Btn_FetchMeter: ", Btn_FetchMeter)

    global Btn_CancelFetchMeter
    colIndex += 1
    Btn_CancelFetchMeter = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelFetchMeter: ", Btn_CancelFetchMeter)

    global Btn_DiscardChangesFetchMeter
    colIndex += 1
    Btn_DiscardChangesFetchMeter = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChangesFetchMeter: ", Btn_DiscardChangesFetchMeter)

    # Add/Edit Customer
    global Exp_AddNewCustomer
    colIndex += 1
    Exp_AddNewCustomer = sheet.row_values(rowIndex)[colIndex]
    print("Exp_AddNewCustomer: ", Exp_AddNewCustomer)

    global Drp_Title
    colIndex += 1
    Drp_Title = sheet.row_values(rowIndex)[colIndex]
    print("Drp_Title: ", Drp_Title)

    global Fld_Initials
    colIndex += 1
    Fld_Initials = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Initials: ", Fld_Initials)

    global Fld_FirstName
    colIndex += 1
    Fld_FirstName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_FirstName: ", Fld_FirstName)

    global Fld_Surname
    colIndex += 1
    Fld_Surname = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Surname: ", Fld_Surname)

    global Fld_IDNumber
    colIndex += 1
    Fld_IDNumber = sheet.row_values(rowIndex)[colIndex]
    print("Fld_IDNumber: ", Fld_IDNumber)

    global Fld_CompanyName
    colIndex += 1
    Fld_CompanyName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CompanyName: ", Fld_CompanyName)

    global Fld_TaxNumber
    colIndex += 1
    Fld_TaxNumber = sheet.row_values(rowIndex)[colIndex]
    print("Fld_TaxNumber: ", Fld_TaxNumber)

    global Fld_EmailAddress1
    colIndex += 1
    Fld_EmailAddress1 = sheet.row_values(rowIndex)[colIndex]
    print("Fld_EmailAddress1: ", Fld_EmailAddress1)

    global Fld_EmailAddress2
    colIndex += 1
    Fld_EmailAddress2 = sheet.row_values(rowIndex)[colIndex]
    print("Fld_EmailAddress2: ", Fld_EmailAddress2)

    global Fld_PhoneNumber1
    colIndex += 1
    Fld_PhoneNumber1 = sheet.row_values(rowIndex)[colIndex]
    print("Fld_PhoneNumber1: ", Fld_PhoneNumber1)

    global Fld_PhoneNumber2
    colIndex += 1
    Fld_PhoneNumber2 = sheet.row_values(rowIndex)[colIndex]
    print("Fld_PhoneNumber2: ", Fld_PhoneNumber2)

    global Fld_AgreementRef
    colIndex += 1
    Fld_AgreementRef = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AgreementRef: ", Fld_AgreementRef)

    global Fld_StartDate
    colIndex += 1
    Fld_StartDate = sheet.row_values(rowIndex)[colIndex]
    print("Fld_StartDate: ", Fld_StartDate)

    global Drp_FreeIssueAuxiliaryAccount
    colIndex += 1
    Drp_FreeIssueAuxiliaryAccount = sheet.row_values(rowIndex)[colIndex]
    print("Drp_FreeIssueAuxiliaryAccount: ", Drp_FreeIssueAuxiliaryAccount)

    global Chk_Billable
    colIndex += 1
    Chk_Billable = sheet.row_values(rowIndex)[colIndex]
    print("Chk_Billable: ", Chk_Billable)

    global Fld_AccountName
    colIndex += 1
    Fld_AccountName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AccountName: ", Fld_AccountName)

    global Btn_SynchronizeBalance_Customer
    colIndex += 1
    Btn_SynchronizeBalance_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_SynchronizeBalance_Customer: ", Btn_SynchronizeBalance_Customer)

    global Fld_LowBalanceThreshold
    colIndex += 1
    Fld_LowBalanceThreshold = sheet.row_values(rowIndex)[colIndex]
    print("Fld_LowBalanceThreshold: ", Fld_LowBalanceThreshold)

    global Fld_NotificationEmailAddresses
    colIndex += 1
    Fld_NotificationEmailAddresses = sheet.row_values(rowIndex)[colIndex]
    print("Fld_NotificationEmailAddresses: ", Fld_NotificationEmailAddresses)

    global Fld_NotificationPhoneNumbers
    colIndex += 1
    Fld_NotificationPhoneNumbers = sheet.row_values(rowIndex)[colIndex]
    print("Fld_NotificationPhoneNumbers: ", Fld_NotificationPhoneNumbers)

    global Exp_PhysicalAddress
    colIndex += 1
    Exp_PhysicalAddress = sheet.row_values(rowIndex)[colIndex]
    print("Exp_PhysicalAddress: ", Exp_PhysicalAddress)

    global Drp_LocationLvl1_Customer
    colIndex += 1
    Drp_LocationLvl1_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl1_Customer: ", Drp_LocationLvl1_Customer)

    global Drp_LocationLvl2_Customer
    colIndex += 1
    Drp_LocationLvl2_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl2_Customer: ", Drp_LocationLvl2_Customer)

    global Drp_LocationLvl3_Customer
    colIndex += 1
    Drp_LocationLvl3_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl3_Customer: ", Drp_LocationLvl3_Customer)

    global Drp_LocationLvl4_Customer
    colIndex += 1
    Drp_LocationLvl4_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl4_Customer: ", Drp_LocationLvl4_Customer)

    global Fld_ErfNumber_Customer
    colIndex += 1
    Fld_ErfNumber_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_ErfNumber_Customer: ", Fld_ErfNumber_Customer)

    global Fld_StreetNumber_Customer
    colIndex += 1
    Fld_StreetNumber_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_StreetNumber_Customer: ", Fld_StreetNumber_Customer)

    global Fld_BuildingName_Customer
    colIndex += 1
    Fld_BuildingName_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_BuildingName_Customer: ", Fld_BuildingName_Customer)

    global Fld_SuiteNumber_Customer
    colIndex += 1
    Fld_SuiteNumber_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_SuiteNumber_Customer: ", Fld_SuiteNumber_Customer)

    global Fld_AddressLine1_Customer
    colIndex += 1
    Fld_AddressLine1_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AddressLine1_Customer: ", Fld_AddressLine1_Customer)

    global Fld_AddressLine2_Customer
    colIndex += 1
    Fld_AddressLine2_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AddressLine2_Customer: ", Fld_AddressLine2_Customer)

    global Fld_AddressLine3_Customer
    colIndex += 1
    Fld_AddressLine3_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AddressLine3_Customer: ", Fld_AddressLine3_Customer)

    global Fld_Latitude_Customer
    colIndex += 1
    Fld_Latitude_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Latitude_Customer: ", Fld_Latitude_Customer)

    global Fld_Longitude_Customer
    colIndex += 1
    Fld_Longitude_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Longitude_Customer: ", Fld_Longitude_Customer)

    global Btn_Save_Customer
    colIndex += 1
    Btn_Save_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Save_Customer: ", Btn_Save_Customer)

    global Btn_Cancel_Customer
    colIndex += 1
    Btn_Cancel_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Cancel_Customer: ", Btn_Cancel_Customer)

    global Btn_DiscardChanges_Customer
    colIndex += 1
    Btn_DiscardChanges_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChanges_Customer: ", Btn_DiscardChanges_Customer)

    global Function_Customer
    colIndex += 1
    Function_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Function_Customer: ", Function_Customer)

    global Element_Customer
    colIndex += 1
    Element_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Element_Customer: ", Element_Customer)

    global Assert_Customer
    colIndex += 1
    Assert_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Assert_Customer: ", Assert_Customer)

    # Fetch Customer
    global Lnk_FetchCustomer
    colIndex += 1
    Lnk_FetchCustomer = sheet.row_values(rowIndex)[colIndex]
    print("Lnk_FetchCustomer: ", Lnk_FetchCustomer)

    global Drp_CustomerSearchBy_Customer
    colIndex += 1
    Drp_CustomerSearchBy_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Drp_CustomerSearchBy_Customer: ", Drp_CustomerSearchBy_Customer)

    global Fld_CustomerSearchByAgrRef
    colIndex += 1
    Fld_CustomerSearchByAgrRef = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CustomerSearchByAgrRef: ", Fld_CustomerSearchByAgrRef)

    global Fld_CustomerSearchByIdNum
    colIndex += 1
    Fld_CustomerSearchByIdNum = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CustomerSearchByIdNum: ", Fld_CustomerSearchByIdNum)

    global Fld_CustomerSearchBySurname_Customer
    colIndex += 1
    Fld_CustomerSearchBySurname_Customer = sheet.row_values(rowIndex)[colIndex]
    print("Fld_CustomerSearchBySurname_Customer: ", Fld_CustomerSearchBySurname_Customer)

    global Btn_FetchCustomer
    colIndex += 1
    Btn_FetchCustomer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_FetchCustomer: ", Btn_FetchCustomer)

    global Btn_CancelFindCustomer
    colIndex += 1
    Btn_CancelFindCustomer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelFindCustomer: ", Btn_CancelFindCustomer)

    global Btn_DiscardChangesFetchCustomer
    colIndex += 1
    Btn_DiscardChangesFetchCustomer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChangesFetchCustomer: ", Btn_DiscardChangesFetchCustomer)

    global Btn_CancelDiscardChangesFetchCustomer
    colIndex += 1
    Btn_CancelDiscardChangesFetchCustomer = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelDiscardChangesFetchCustomer: ", Btn_CancelDiscardChangesFetchCustomer)

    # Edit/Add Usage Point
    global Exp_AddUsagePoint
    colIndex += 1
    Exp_AddUsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("Exp_AddUsagePoint: ", Exp_AddUsagePoint)

    global Chk_Inactive
    colIndex += 1
    Chk_Inactive = sheet.row_values(rowIndex)[colIndex]
    print("Chk_Inactive: ", Chk_Inactive)

    global Btn_InactiveCloseAlertUP
    colIndex += 1
    Btn_InactiveCloseAlertUP = sheet.row_values(rowIndex)[colIndex]
    print("Btn_InactiveCloseAlertUP: ", Btn_InactiveCloseAlertUP)

    global Fld_MeterInstallationDataTime
    colIndex += 1
    Fld_MeterInstallationDataTime = sheet.row_values(rowIndex)[colIndex]
    print("Fld_MeterInstallationDataTime: ", Fld_MeterInstallationDataTime)

    global Fld_UsagePointName
    colIndex += 1
    Fld_UsagePointName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_UsagePointName: ", Fld_UsagePointName)

    global Drp_PricingStructure1
    colIndex += 1
    Drp_PricingStructure1 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_PricingStructure1: ", Drp_PricingStructure1)

    global Fld_StartDate1
    colIndex += 1
    Fld_StartDate1 = sheet.row_values(rowIndex)[colIndex]
    print("Fld_StartDate1: ", Fld_StartDate1)

    global Drp_PricingStructure2
    colIndex += 1
    Drp_PricingStructure2 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_PricingStructure2: ", Drp_PricingStructure2)

    global Fld_StartDate2
    colIndex += 1
    Fld_StartDate2 = sheet.row_values(rowIndex)[colIndex]
    print("Fld_StartDate2: ", Fld_StartDate2)

    global Btn_Delete
    colIndex += 1
    Btn_Delete = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Delete: ", Btn_Delete)

    global Btn_ViewAll
    colIndex += 1
    Btn_ViewAll = sheet.row_values(rowIndex)[colIndex]
    print("Btn_ViewAll: ", Btn_ViewAll)

    global Fld_PriceChangeReason
    colIndex += 1
    Fld_PriceChangeReason = sheet.row_values(rowIndex)[colIndex]
    print("Fld_PriceChangeReason: ", Fld_PriceChangeReason)

    global Btn_ViewOutStandingCharges
    colIndex += 1
    Btn_ViewOutStandingCharges = sheet.row_values(rowIndex)[colIndex]
    print("Btn_ViewOutStandingCharges: ", Btn_ViewOutStandingCharges)

    global Btn_SendMeterInspectionRequest
    colIndex += 1
    Btn_SendMeterInspectionRequest = sheet.row_values(rowIndex)[colIndex]
    print("Btn_SendMeterInspectionRequest: ", Btn_SendMeterInspectionRequest)

    global Drp_BlockingType
    colIndex += 1
    Drp_BlockingType = sheet.row_values(rowIndex)[colIndex]
    print("Drp_BlockingType: ", Drp_BlockingType)

    global Fld_UnitsAccountName
    colIndex += 1
    Fld_UnitsAccountName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_UnitsAccountName: ", Fld_UnitsAccountName)

    global Btn_SynchronizeBalanceUP
    colIndex += 1
    Btn_SynchronizeBalanceUP = sheet.row_values(rowIndex)[colIndex]
    print("Btn_SynchronizeBalanceUP: ", Btn_SynchronizeBalanceUP)

    global Btn_CloseAlertUP
    colIndex += 1
    Btn_CloseAlertUP = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CloseAlertUP: ", Btn_CloseAlertUP)

    global Fld_UnitsLowBalanceThreshold
    colIndex += 1
    Fld_UnitsLowBalanceThreshold = sheet.row_values(rowIndex)[colIndex]
    print("Fld_UnitsLowBalanceThreshold: ", Fld_UnitsLowBalanceThreshold)

    global Fld_NotificationEmailAddressesUP
    colIndex += 1
    Fld_NotificationEmailAddressesUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_NotificationEmailAddressesUP: ", Fld_NotificationEmailAddressesUP)

    global Fld_NotificationPhoneNumbersUP
    colIndex += 1
    Fld_NotificationPhoneNumbersUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_NotificationPhoneNumbersUP: ", Fld_NotificationPhoneNumbersUP)

    global Exp_PhysicalAddressUP
    colIndex += 1
    Exp_PhysicalAddressUP = sheet.row_values(rowIndex)[colIndex]
    print("Exp_PhysicalAddressUP: ", Exp_PhysicalAddressUP)

    global Drp_LocationLvl1UP
    colIndex += 1
    Drp_LocationLvl1UP = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl1UP: ", Drp_LocationLvl1UP)

    global Drp_LocationLvl2UP
    colIndex += 1
    Drp_LocationLvl2UP = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl2UP: ", Drp_LocationLvl2UP)

    global Drp_LocationLvl3UP
    colIndex += 1
    Drp_LocationLvl3UP = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl3UP: ", Drp_LocationLvl3UP)

    global Drp_LocationLvl4UP
    colIndex += 1
    Drp_LocationLvl4UP = sheet.row_values(rowIndex)[colIndex]
    print("Drp_LocationLvl4UP: ", Drp_LocationLvl4UP)

    global Fld_ErfNumberUP
    colIndex += 1
    Fld_ErfNumberUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_ErfNumberUP: ", Fld_ErfNumberUP)

    global Fld_StreetNumberUP
    colIndex += 1
    Fld_StreetNumberUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_StreetNumberUP: ", Fld_StreetNumberUP)

    global Fld_BuildingNameUP
    colIndex += 1
    Fld_BuildingNameUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_BuildingNameUP: ", Fld_BuildingNameUP)

    global Fld_SuiteNumberUP
    colIndex += 1
    Fld_SuiteNumberUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_SuiteNumberUP: ", Fld_SuiteNumberUP)

    global Fld_AddressLine1UP
    colIndex += 1
    Fld_AddressLine1UP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AddressLine1UP: ", Fld_AddressLine1UP)

    global Fld_AddressLine2UP
    colIndex += 1
    Fld_AddressLine2UP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AddressLine2UP: ", Fld_AddressLine2UP)

    global Fld_AddressLine3UP
    colIndex += 1
    Fld_AddressLine3UP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_AddressLine3UP: ", Fld_AddressLine3UP)

    global Fld_LatitudeUP
    colIndex += 1
    Fld_LatitudeUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_LatitudeUP: ", Fld_LatitudeUP)

    global Fld_LongitudeUP
    colIndex += 1
    Fld_LongitudeUP = sheet.row_values(rowIndex)[colIndex]
    print("Fld_LongitudeUP: ", Fld_LongitudeUP)

    global Exp_UsagePointGroups
    colIndex += 1
    Exp_UsagePointGroups = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointGroups: ", Exp_UsagePointGroups)

    global Drp_UPG1Lvl1
    colIndex += 1
    Drp_UPG1Lvl1 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_UPG1Lvl1: ", Drp_UPG1Lvl1)

    global Drp_UPG1Lvl2
    colIndex += 1
    Drp_UPG1Lvl2 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_UPG1Lvl2: ", Drp_UPG1Lvl2)

    global Drp_UPG1Lvl3
    colIndex += 1
    Drp_UPG1Lvl3 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_UPG1Lvl3: ", Drp_UPG1Lvl3)

    global Drp_UPG2Lvl1
    colIndex += 1
    Drp_UPG2Lvl1 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_UPG2Lvl1: ", Drp_UPG2Lvl1)

    global Drp_UPG2Lvl2
    colIndex += 1
    Drp_UPG2Lvl2 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_UPG2Lvl2: ", Drp_UPG2Lvl2)

    global Drp_UPG2Lvl3
    colIndex += 1
    Drp_UPG2Lvl3 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_UPG2Lvl3: ", Drp_UPG2Lvl3)

    global Drp_NonDisconnectLvl1
    colIndex += 1
    Drp_NonDisconnectLvl1 = sheet.row_values(rowIndex)[colIndex]
    print("Drp_NonDisconnectLvl1: ", Drp_NonDisconnectLvl1)

    global Btn_SaveUP
    colIndex += 1
    Btn_SaveUP = sheet.row_values(rowIndex)[colIndex]
    print("Btn_SaveUP: ", Btn_SaveUP)

    global Btn_CancelUP
    colIndex += 1
    Btn_CancelUP = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelUP: ", Btn_CancelUP)

    global Btn_CalcTariff
    colIndex += 1
    Btn_CalcTariff = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CalcTariff: ", Btn_CalcTariff)

    global Btn_DiscardChangesUP
    colIndex += 1
    Btn_DiscardChangesUP = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChangesUP: ", Btn_DiscardChangesUP)

    global Btn_CancelDiscardChanges
    colIndex += 1
    Btn_CancelDiscardChanges = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelDiscardChanges: ", Btn_CancelDiscardChanges)

    global Function_UP
    colIndex += 1
    Function_UP = sheet.row_values(rowIndex)[colIndex]
    print("Function_UP: ", Function_UP)

    global Element_UP
    colIndex += 1
    Element_UP = sheet.row_values(rowIndex)[colIndex]
    print("Element_UP: ", Element_UP)

    global Assert_UP
    colIndex += 1
    Assert_UP = sheet.row_values(rowIndex)[colIndex]
    print("Assert_UP: ", Assert_UP)

    global Btn_ConfirmDateAndTime
    colIndex += 1
    Btn_ConfirmDateAndTime = sheet.row_values(rowIndex)[colIndex]
    print("Btn_ConfirmDateAndTime: ", Btn_ConfirmDateAndTime)

    global Btn_CancelConfirmDateAndTime
    colIndex += 1
    Btn_CancelConfirmDateAndTime = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelConfirmDateAndTime: ", Btn_CancelConfirmDateAndTime)

    global Btn_Yes
    colIndex += 1
    Btn_Yes = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Yes: ", Btn_Yes)

    global Btn_No
    colIndex += 1
    Btn_No = sheet.row_values(rowIndex)[colIndex]
    print("Btn_No: ", Btn_No)

    global Fld_IntialReading
    colIndex += 1
    Fld_IntialReading = sheet.row_values(rowIndex)[colIndex]
    print("Fld_IntialReading: ", Fld_IntialReading)

    global Btn_InitialReadingSave
    colIndex += 1
    Btn_InitialReadingSave = sheet.row_values(rowIndex)[colIndex]
    print("Btn_InitialReadingSave: ", Btn_InitialReadingSave)

    global Btn_InitialReadingCancel
    colIndex += 1
    Btn_InitialReadingCancel = sheet.row_values(rowIndex)[colIndex]
    print("Btn_InitialReadingCancel: ", Btn_InitialReadingCancel)


    # Fetch Usage Point

    global Lnk_FetchUsagePoint
    colIndex += 1
    Lnk_FetchUsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("Lnk_FetchUsagePoint: ", Lnk_FetchUsagePoint)

    global Fld_EnterUsagePointName
    colIndex += 1
    Fld_EnterUsagePointName = sheet.row_values(rowIndex)[colIndex]
    print("Fld_EnterUsagePointName: ", Fld_EnterUsagePointName)

    global Btn_FetchUsagePoint
    colIndex += 1
    Btn_FetchUsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("Btn_FetchUsagePoint: ", Btn_FetchUsagePoint)

    global Btn_cancelFindUsagePoint
    colIndex += 1
    Btn_cancelFindUsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("Btn_cancelFindUsagePoint: ", Btn_cancelFindUsagePoint)

    global Btn_DiscardChangesFindUsagePoint
    colIndex += 1
    Btn_DiscardChangesFindUsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("Btn_DiscardChangesFindUsagePoint: ", Btn_DiscardChangesFindUsagePoint)

    global Btn_CancelDiscardChangesFindUsagePoint
    colIndex += 1
    Btn_CancelDiscardChangesFindUsagePoint = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CancelDiscardChangesFindUsagePoint: ", Btn_CancelDiscardChangesFindUsagePoint)

    global Btn_CloseTab_AddNewMeter
    colIndex += 1
    Btn_CloseTab_AddNewMeter = sheet.row_values(rowIndex)[colIndex]
    print("Btn_CloseTab_AddNewMeter: ", Btn_CloseTab_AddNewMeter)


def Framework():
    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return
 # Search Options
    if MnuMainSearch != "":
        print("Do Something with MnuMainSearch: ", MnuMainSearch)
        ElementToBeClickableByID_Click("searchMenu", MnuMainSearch)

    if Fld_SearchByMeterNumber != "":
        print("Do Something with Fld_SearchByMeterNumber: ", Fld_SearchByMeterNumber)
        ElementToBeClickableByID_SendKeys("meterSearchBox", Fld_SearchByMeterNumber)

    if Btn_SearchByMeterNumber != "":
        print("Do Something with Btn_SearchByMeterNumber: ", Btn_SearchByMeterNumber)
        ElementToBeClickableByID_Click("meterSearchImage", Btn_SearchByMeterNumber)

    if Fld_SearchByUsagePointName != "":
        print("Do Something with Fld_SearchByUsagePointName: ", Fld_SearchByUsagePointName)
        ElementToBeClickableByID_SendKeys("usagePointSearchBox", Fld_SearchByUsagePointName)

    if Btn_SearchByUsagePointName != "":
        print("Do Something with Btn_SearchByUsagePointName: ", Btn_SearchByUsagePointName)
        ElementToBeClickableByID_Click("usagePointSearchImage", Btn_SearchByUsagePointName)

    if Drp_CustomerSearchBy != "":
        print("Do Something with Drp_CustomerSearchBy: ", Drp_CustomerSearchBy)
        ElementToBeClickableByID_DropDown("customerSearchTypeBox", Drp_CustomerSearchBy)

    if Fld_CustomerSearchBySurname != "":
        print("Do Something with Fld_CustomerSearchBySurname: ", Fld_CustomerSearchBySurname)
        ElementToBeClickableByID_SendKeys("suggestBoxCustomerSurname", Fld_CustomerSearchBySurname)

    if Fld_CustomerSearchByAgreementRef != "":
        print("Do Something with Fld_CustomerSearchByAgreementRef: ", Fld_CustomerSearchByAgreementRef)
        ElementToBeClickableByID_SendKeys("customerAgreementSearchBox", Fld_CustomerSearchByAgreementRef)

    if Fld_CustomerSearchByAccountName != "":
        print("Do Something with Fld_CustomerSearchByAccountName: ", Fld_CustomerSearchByAccountName)
        ElementToBeClickableByID_SendKeys("accountNameSearchBox", Fld_CustomerSearchByAccountName)

    if Fld_CustomerSearchByIDNumber != "":
        print("Do Something with Fld_CustomerSearchByIDNumber: ", Fld_CustomerSearchByIDNumber)
        ElementToBeClickableByID_SendKeys("suggestBoxCustomerIdNumber", Fld_CustomerSearchByIDNumber)

    if Btn_CustomerSearchBy != "":
        print("Do Something with Btn_CustomerSearchBy: ", Btn_CustomerSearchBy)
        ElementToBeClickableByID_Click("customerSearchImage", Btn_CustomerSearchBy)
    """
    if Btn_CloseTab_Search != "":
        browser.close()
    """
# Menus
    if MnuMain_Meter != "":
        print("Do Something with MnuMain_Meter: ", MnuMain_Meter)
        ElementToBeClickableByID_executeClick("meterMenu", MnuMain_Meter)

    if MnuSub_Meter != "":
        print("Do Something with MnuSub_Meter: ", MnuSub_Meter)
        ElementToBeClickableByID_executeClick("addMeterLink-wrapper", MnuSub_Meter)

# Add/Edit Meter
    if Drp_SelectMeterModel != "":
        print("Do Something with Drp_SelectMeterModel: ", Drp_SelectMeterModel)
        ElementToBeClickableByID_DropDown("meterModelBox", Drp_SelectMeterModel)

    if Fld_MeterNumber != "":
        print("Do Something with Fld_MeterNumber: ", Fld_MeterNumber)
        ElementToBeClickableByID_SendKeys("meterNumBox", Fld_MeterNumber)

    if Fld_SerialNumber != "":
        print("Do Something with Fld_SerialNumber: ", Fld_SerialNumber)
        ElementToBeClickableByID_SendKeys("meterSerialBox", Fld_SerialNumber)

    if Fld_UniqueID != "":
        print("Do Something with Fld_UniqueID: ",Fld_UniqueID)
        ElementToBeClickableByID_SendKeys("meterMridBox", Fld_UniqueID)

    if Chk_ExternalUniqueID != "":
        print("Do Something with Chk_ExternalUniqueID: ", Chk_ExternalUniqueID)
        ElementToBeClickableByID_Check("meterMridEditBox-label", Chk_ExternalUniqueID)

    if Drp_Algorithmcode != "":
        print("Do Something with Drp_Algorithmcode: ", Drp_Algorithmcode)
        ElementToBeClickableByID_DropDown("meterAlgBox", Drp_Algorithmcode)

    if Drp_TokenTechCode != "":
        print("Do Something with Drp_TokenTechCode: ", Drp_TokenTechCode)
        ElementToBeClickableByID_DropDown("meterTokenTechBox", Drp_TokenTechCode)

    if Drp_CurrentSupplyGroupCode != "":
        print("Do Something with Drp_CurrentSupplyGroupCode: ", Drp_CurrentSupplyGroupCode)
        ElementToBeClickableByID_DropDown("meterSupplyGroupBox", Drp_CurrentSupplyGroupCode)

    if Fld_CurrentTariffIndex != "":
        print("Do Something with Fld_CurrentTariffIndex: ", Fld_CurrentTariffIndex)
        ElementToBeClickableByID_SendKeys("meterTariffIndexBox", Fld_CurrentTariffIndex)

    if Chk_ThreeKeyChangeTokensRequired != "":
        print("Do Something with Chk_ThreeKeyChangeTokensRequired: ", Chk_ThreeKeyChangeTokensRequired)
        ElementToBeClickableByID_Check("meterThreeTokensChckbx-input", Chk_ThreeKeyChangeTokensRequired)

    if Drp_PowerLimit != "":
        print("Do Something with Drp_PowerLimit: ", Drp_PowerLimit)
        ElementToBeClickableByXPATH_DropDown('id("meterPanel")/div[8]/fieldset[@class="formGroupPanel_captionPanel"]/table[1]/tbody[1]/tr[1]/td[1]/div[@class="formGroupPanel_internalPanel"]/div[1]/div[@class="formRowPanel_panel"]/div[@class="formElement_panel"]/div[@class="formElement_innerPanel"]/table[1]/tbody[1]/tr[1]/td[1]/div[@class="gwt-ListBox-ipay"]/select[@class="gwt-ListBox"]', Drp_PowerLimit)

    if Drp_GeneratePowerLimitTokens != "":
        print("Do Something with Drp_GeneratePowerLimitTokens: ", Drp_GeneratePowerLimitTokens)
        ElementToBeClickableByID_DropDown("generatePowerLimitToken", Drp_GeneratePowerLimitTokens)

    if Drp_MeterStore != "":
        print("Do Something with Drp_MeterStore: ", Drp_MeterStore)
        ElementToBeClickableByID_DropDown("meterStoreBox", Drp_MeterStore)

    if Btn_Save_Meter != "":
        print("Do Something with Btn_Save_Meter: ", Btn_Save_Meter)
        ElementToBeClickableByID_Click("meterSaveButton", Btn_Save_Meter)

    if Btn_Cancel_Meter != "":
        print("Do Something with Btn_Cancel_Meter: ", Btn_Cancel_Meter)
        ElementToBeClickableByID_Click("meterCancelButton", Btn_Cancel_Meter)

    if Function_Meter != "":
        print("Do Something with Function_Meter: ", Function_Meter)

        if Element_Meter != "":
            print("Do Something with Element_Meter: ", Element_Meter)

            if Assert_Meter != "":
                print("Do Something with Assert_Meter :", Assert_Meter)

                globals()[Function_Meter](Element_Meter, Assert_Meter)

    if Btn_DiscardChangesNewMeter != "":
        print("Do Something with Btn_DiscardChangesNewMeter: ", Btn_DiscardChangesNewMeter)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChangesNewMeter)


# Fetch Meter
    if Lnk_FetchMeterFromDeviceStore != "":
        print("Do Something with Lnk_FetchMeterFromDeviceStore: ", Lnk_FetchMeterFromDeviceStore)
        ElementToBeClickableByID_SendKeys("assignMeterLink", Lnk_FetchMeterFromDeviceStore)

    if Fld_EnterMeterNumberFetchMeter != "":
        print("Do Something with Fld_EnterMeterNumberFetchMeter: ", Fld_EnterMeterNumberFetchMeter)
        ElementToBeClickableByXPATH_SendKeys('id("assignMeterDialogBox-content")/div[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[4]/td[1]/input[@class="gwt-TextBox-ipay"]', Fld_EnterMeterNumberFetchMeter)

    if Btn_FetchMeter != "":
        print("Do Something with Btn_FetchMeter: ", Btn_FetchMeter)
        ElementToBeClickableByID_Click("replaceMeterButton", Btn_FetchMeter)

    if Btn_CancelFetchMeter != "":
        print("Do Something with Drp_MeterStore: ", Btn_CancelFetchMeter)
        ElementToBeClickableByID_Click("cancelDefault", Btn_CancelFetchMeter)

    if Btn_DiscardChangesFetchMeter != "":
        print("Do Something with Btn_DiscardChangesFetchMeter: ", Btn_DiscardChangesFetchMeter)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChangesFetchMeter)
    #click at random place
    if Btn_Save_Meter != "":
        print("Click at random place if Btn_Save_Meter is clicked: ", Btn_Save_Meter)
        ElementToBeClickableByXPATH_Click('id("metercomponent")/table[@id="meterDisclosurePanel"]/tbody[1]/tr[2]/td[1]/div[1]/div[@class="content"]/div[@class="disclosureContent"]', Btn_Save_Meter)


# Add/Edit Customer
    if Exp_AddNewCustomer != "":
        print("Do Something with Exp_AddNewCustomer: ", Exp_AddNewCustomer)
        ElementToBeClickableByXPATH_Click('id("customerDisclosurePanel-header")/div[@class="gwt-DisclosurePanel-component .header"]', Exp_AddNewCustomer)

    if Drp_Title != "":
        print("Do Something with Drp_Title: ", Drp_Title)
        ElementToBeClickableByID_DropDown("titleListBox", Drp_Title)

    if Fld_Initials != "":
        print("Do Something with Fld_Initials: ", Fld_Initials)
        ElementToBeClickableByID_SendKeys("initialsBox", Fld_Initials)

    if Fld_FirstName != "":
        print("Do Something with Fld_FirstName: ", Fld_FirstName)
        ElementToBeClickableByID_SendKeys("nameBox", Fld_FirstName)

    if Fld_Surname != "":
        print("Do Something with Fld_Surname: ", Fld_Surname)
        ElementToBeClickableByID_SendKeys("surnameBox", Fld_Surname)

    if Fld_IDNumber != "":
        print("Do Something with Fld_IDNumber: ", Fld_IDNumber)
        ElementToBeClickableByID_SendKeys("idNumberBox", Fld_IDNumber)

    if Fld_CompanyName != "":
        print("Do Something with Fld_CompanyName: ", Fld_CompanyName)
        ElementToBeClickableByID_SendKeys("companyNameBox", Fld_CompanyName)

    if Fld_TaxNumber != "":
        print("Do Something with Fld_TaxNumber: ", Fld_TaxNumber)
        ElementToBeClickableByID_SendKeys("taxNumberBox", Fld_TaxNumber)

    if Fld_EmailAddress1 != "":
        print("Do Something with Fld_EmailAddress1: ", Fld_EmailAddress1)
        ElementToBeClickableByID_SendKeys("emailBox", Fld_EmailAddress1)

    if Fld_EmailAddress2 != "":
        print("Do Something with Fld_FirstName: ", Fld_EmailAddress2)
        ElementToBeClickableByID_SendKeys("emailBox2", Fld_EmailAddress2)

    if Fld_PhoneNumber1 != "":
        print("Do Something with Fld_PhoneNumber1: ", Fld_PhoneNumber1)
        ElementToBeClickableByID_SendKeys("phoneBox", Fld_PhoneNumber1)

    if Fld_PhoneNumber2 != "":
        print("Do Something with Fld_PhoneNumber2: ", Fld_PhoneNumber2)
        ElementToBeClickableByID_SendKeys("phoneBox2", Fld_PhoneNumber2)

    if Fld_AgreementRef != "":
        print("Do Something with Fld_AgreementRef: ", Fld_AgreementRef)
        ElementToBeClickableByID_SendKeys("agreementRefBox", Fld_AgreementRef)

    if Fld_StartDate != "":
        print("Do Something with Fld_StartDate: ", Fld_StartDate)
        ElementToBeClickableByID_SendKeys("startDateBox", Fld_StartDate)

    if Drp_FreeIssueAuxiliaryAccount != "":
        print("Do Something with Drp_FreeIssueAuxiliaryAccount: ", Drp_FreeIssueAuxiliaryAccount)
        ElementToBeClickableByID_DropDown('id("customercomponent")/tbody[1]/tr[2]/td[1]/div[1]/div[@class="content"]/div[@class="disclosureContent"]/div[@class="formElementsPanel"]/div[4]/fieldset[@class="formGroupPanel_captionPanel"]/table[1]/tbody[1]/tr[1]/td[1]/div[@class="formGroupPanel_internalPanel"]/div[1]/div[@class="formRowPanel_panel"]/div[@class="formElement_panel"]', Drp_FreeIssueAuxiliaryAccount)

    if Chk_Billable != "":
        print("Do Something with Chk_Billable: ", Chk_Billable)
        ElementToBeClickableByID_Check("billableBox-label", Chk_Billable)

    if Fld_AccountName != "":
        print("Do Something with Fld_AccountName: ", Fld_AccountName)
        ElementToBeClickableByID_SendKeys("accountNameBox", Fld_AccountName)

    if Btn_SynchronizeBalance_Customer != "":
        print("Do Something with Btn_SynchronizeBalance_Customer: ", Btn_SynchronizeBalance_Customer)
        ElementToBeClickableByID_Click("btnSync", Btn_SynchronizeBalance_Customer)

    if Fld_LowBalanceThreshold != "":
        print("Do Something with Fld_LowBalanceThreshold: ", Fld_LowBalanceThreshold)
        ElementToBeClickableByID_SendKeys("lowBalanceThresholdBox", Fld_LowBalanceThreshold)

    if Fld_NotificationEmailAddresses != "":
        print("Do Something with Fld_NotificationEmailAddresses: ", Fld_NotificationEmailAddresses)
        ElementToBeClickableByID_SendKeys("notificationEmailBox", Fld_NotificationEmailAddresses)

    if Fld_NotificationPhoneNumbers != "":
        print("Do Something with Fld_NotificationPhoneNumbers: ", Fld_NotificationPhoneNumbers)
        ElementToBeClickableByID_SendKeys("notificationPhoneBox", Fld_NotificationPhoneNumbers)

    if Exp_PhysicalAddress != "":
        print("Do Something with Exp_PhysicalAddress: ", Exp_PhysicalAddress)
        ElementToBeClickableByID_Click("physicalAddressPanel-header", Exp_PhysicalAddress)

    if Drp_LocationLvl1_Customer != "":
        print("Do Something with Drp_LocationLvl1_Customer: ", Drp_LocationLvl1_Customer)
        ElementToBeClickableByID_DropDown("selectionDataBox0", Drp_LocationLvl1_Customer)

    if Drp_LocationLvl2_Customer != "":
        print("Do Something with Drp_LocationLvl2_Customer: ", Drp_LocationLvl2_Customer)
        ElementToBeClickableByID_DropDown("selectionDataBox1", Drp_LocationLvl2_Customer)

    if Drp_LocationLvl3_Customer != "":
        print("Do Something with Drp_LocationLvl3_Customer: ", Drp_LocationLvl3_Customer)
        ElementToBeClickableByID_DropDown("selectionDataBox2", Drp_LocationLvl3_Customer)

    if Drp_LocationLvl4_Customer != "":
        print("Do Something with Drp_LocationLvl4_Customer: ", Drp_LocationLvl4_Customer)
        ElementToBeClickableByID_DropDown("selectionDataBox3", Drp_LocationLvl4_Customer)

    if Fld_ErfNumber_Customer != "":
        print("Do Something with Fld_ErfNumber_Customer: ", Fld_ErfNumber_Customer)
        ElementToBeClickableByID_SendKeys("erfNumberBox", Fld_ErfNumber_Customer)

    if Fld_StreetNumber_Customer != "":
        print("Do Something with Fld_StreetNumber_Customer: ", Fld_StreetNumber_Customer)
        ElementToBeClickableByID_SendKeys("streetNumberBox", Fld_StreetNumber_Customer)

    if Fld_BuildingName_Customer != "":
        print("Do Something with Fld_BuildingName_Customer: ", Fld_BuildingName_Customer)
        ElementToBeClickableByID_SendKeys("buildingNameBox", Fld_BuildingName_Customer)

    if Fld_SuiteNumber_Customer != "":
        print("Do Something with Fld_SuiteNumber_Customer: ", Fld_SuiteNumber_Customer)
        ElementToBeClickableByID_SendKeys("suiteNumberBox", Fld_SuiteNumber_Customer)

    if Fld_AddressLine1_Customer != "":
        print("Do Something with Fld_AddressLine1_Customer: ", Fld_AddressLine1_Customer)
        ElementToBeClickableByID_SendKeys("addressBox1", Fld_AddressLine1_Customer)

    if Fld_AddressLine2_Customer != "":
        print("Do Something with Fld_AddressLine2_Customer: ", Fld_AddressLine2_Customer)
        ElementToBeClickableByID_SendKeys("addressBox2", Fld_AddressLine2_Customer)

    if Fld_AddressLine3_Customer != "":
        print("Do Something with Fld_AddressLine3_Customer: ", Fld_AddressLine3_Customer)
        ElementToBeClickableByID_SendKeys("addressBox3", Fld_AddressLine3_Customer)

    if Fld_Latitude_Customer != "":
        print("Do Something with Fld_Latitude_Customer: ", Fld_Latitude_Customer)
        ElementToBeClickableByID_SendKeys("latitudeBox", Fld_Latitude_Customer)

    if Fld_Longitude_Customer != "":
        print("Do Something with Fld_Longitude_Customer: ", Fld_Longitude_Customer)
        ElementToBeClickableByID_SendKeys("longitudeBox", Fld_Longitude_Customer)

    if Btn_Save_Customer != "":
        print("Do Something with Btn_Save_Customer: ", Btn_Save_Customer)
        ElementToBeClickableByID_Click("customerSaveButton", Btn_Save_Customer)

    if Btn_Cancel_Customer != "":
        print("Do Something with Btn_Cancel_Customer: ", Btn_Cancel_Customer)
        ElementToBeClickableByID_Click("customerCancelButton", Btn_Cancel_Customer)

    if Btn_DiscardChanges_Customer != "":
        print("Do Something with Btn_DiscardChanges_Customer: ", Btn_DiscardChanges_Customer)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChanges_Customer)

    if Function_Customer != "":
        print("Do Something with Function_Customer: ", Function_Customer)

        if Element_Customer != "":
            print("Do Something with Element_Customer: ", Element_Customer)

            if Assert_Customer != "":
                print("Do Something with Assert_Customer :", Assert_Customer)

                globals()[Function_Customer](Element_Customer, Assert_Customer)




# Fetch Customer
    if Lnk_FetchCustomer != "":
        print("Do Something with Lnk_FetchCustomer: ", Lnk_FetchCustomer)
        ElementToBeClickableByID_Click("assignCustomerLabel", Lnk_FetchCustomer)

    if Drp_CustomerSearchBy_Customer != "":
        print("Do Something with Drp_CustomerSearchBy_Customer: ", Drp_CustomerSearchBy_Customer)
        ElementToBeClickableByID_DropDown("searchCriterionOptions", Drp_CustomerSearchBy_Customer )

    if Fld_CustomerSearchByAgrRef != "":
        print("Do Something with Fld_CustomerSearchByAgrRef: ", Fld_CustomerSearchByAgrRef)
        ElementToBeClickableByID_SendKeys("assignCustomerBoxAgrRef", Fld_CustomerSearchByAgrRef)

    if Fld_CustomerSearchByIdNum != "":
        print("Do Something with Fld_CustomerSearchByIdNum: ", Fld_CustomerSearchByIdNum)
        ElementToBeClickableByID_SendKeys("assignCustomerBoxIdNum", Fld_CustomerSearchByIdNum)

    if Fld_CustomerSearchBySurname_Customer != "":
        print("Do Something with Fld_CustomerSearchBySurname_Customer: ", Fld_CustomerSearchBySurname_Customer)
        ElementToBeClickableByID_SendKeys("assignCustomerBoxSurname", Fld_CustomerSearchBySurname_Customer)

    if Btn_FetchCustomer != "":
        print("Do Something with Btn_FetchCustomer: ", Btn_FetchCustomer)
        ElementToBeClickableByID_Click("assignCustomerButton", Btn_FetchCustomer)

    if Btn_CancelFindCustomer != "":
        print("Do Something with Btn_CancelFindCustomer: ", Btn_CancelFindCustomer)
        ElementToBeClickableByID_Click("btnCancel", Btn_CancelFindCustomer)

    if Btn_DiscardChangesFetchCustomer != "":
        print("Do Something with Btn_DiscardChangesFetchCustomer: ", Btn_DiscardChangesFetchCustomer)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChangesFetchCustomer)

    if Btn_CancelDiscardChangesFetchCustomer != "":
        print("Do Something with Btn_CancelDiscardChangesFetchCustomer: ", Btn_CancelDiscardChangesFetchCustomer)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_CancelDiscardChangesFetchCustomer)

    #close the coustomer expand
    if Exp_AddNewCustomer != "":
        print("Do Something with Exp_AddNewCustomer: ", Exp_AddNewCustomer)
        ElementToBeClickableByXPATH_Click('id("customerDisclosurePanel-header")/div[@class="gwt-DisclosurePanel-component .header"]', Exp_AddNewCustomer)



# Edit/Add Usage Point

    if Exp_AddUsagePoint != "":
        print("Do Something with Exp_AddUsagePoint: ", Exp_AddUsagePoint)
        ElementToBeClickableByID_Click("usagePointDisclosurePanel", Exp_AddUsagePoint)

    if Chk_Inactive != "":
        print("Do Something with Chk_Inactive: ", Chk_Inactive)
        ElementToBeClickableByID_Check("activeBox-input", Chk_Inactive)

    if Btn_InactiveCloseAlertUP != "":
        print("Btn_InactiveCloseAlertUP: ", Btn_InactiveCloseAlertUP)
        ElementToBeClickableByID_Click("informationErrorPanelCloseBtn", Btn_InactiveCloseAlertUP)

    if Fld_MeterInstallationDataTime != "":
        print("Do Something with Fld_MeterInstallationDataTime: ", Fld_MeterInstallationDataTime)
        ElementToBeClickableByID_SendKeys("dtbxMeterInstallationDate", Fld_MeterInstallationDataTime)

    if Fld_UsagePointName != "":
        print("Do Something with Fld_UsagePointName: ", Fld_UsagePointName)
        ElementToBeClickableByID_SendKeys("usagePointNameBox", Fld_UsagePointName)

    if Drp_PricingStructure1 != "":
        print("Do Something with Drp_PricingStructure1: ", Drp_PricingStructure1)
        ElementToBeClickableByID_DropDown("currentPricingStructureListbx", Drp_PricingStructure1)

    if Fld_StartDate1 != "":
        print("Do Something with Fld_StartDate1: ", Fld_StartDate1)
        ElementToBeClickableByID_SendKeys("currentPricingStructureStartDate", Fld_StartDate1)

    if Drp_PricingStructure2 != "":
        print("Do Something with Drp_PricingStructure2: ", Drp_PricingStructure2)
        ElementToBeClickableByID_DropDown("futurePricingStructureListbx", Drp_PricingStructure2)

    if Fld_StartDate2 != "":
        print("Do Something with Fld_StartDate2: ", Fld_StartDate2)
        ElementToBeClickableByID_SendKeys("futurePricingStructureStartDate", Fld_StartDate2)

    if Btn_Delete != "":
        print("Do Something with Btn_Delete: ", Btn_Delete)
        ElementToBeClickableByID_Click("pricingStructureDeleteButton", Btn_Delete)

    if Btn_ViewAll != "":
        print("Do Something with Btn_ViewAll: ", Btn_ViewAll)
        ElementToBeClickableByID_Click("pricingStructureViewAllButton", Btn_ViewAll)

    if Fld_PriceChangeReason != "":
        print("Do Something with Fld_PriceChangeReason: ", Fld_PriceChangeReason)
        ElementToBeClickableByID_SendKeys("reasonsTextBox", Fld_PriceChangeReason)

    if Btn_ViewOutStandingCharges != "":
        print("Do Something with Btn_ViewOutStandingCharges: ", Btn_ViewOutStandingCharges)
        ElementToBeClickableByID_Click("usagePointpChargeViewButton", Btn_ViewOutStandingCharges)


    if Btn_SendMeterInspectionRequest != "":
        print("Do Something with Btn_SendMeterInspectionRequest: ", Btn_SendMeterInspectionRequest)
        ElementToBeClickableByID_Click("usagePointpMeterInspectionButton", Btn_SendMeterInspectionRequest)

    if Drp_BlockingType != "":
        print("Do Something with Drp_BlockingType: ", Drp_BlockingType)
        ElementToBeClickableByID_DropDown("lstbxUpBlockingType", Drp_BlockingType)

    if Fld_UnitsAccountName != "":
        print("Do Something with Fld_UnitsAccountName: ", Fld_UnitsAccountName)
        ElementToBeClickableByID_SendKeys("accountNameBox", Fld_UnitsAccountName)

    if Btn_SynchronizeBalanceUP != "":
        print("Do Something with Btn_SynchronizeBalanceUP: ", Btn_SynchronizeBalanceUP)
        ElementToBeClickableByID_Click("btnSync")

    if Btn_CloseAlertUP != "":
        print("Do Something with Btn_CloseAlertUP: ", Btn_CloseAlertUP)
        ElementToBeClickableByID_Click("informationErrorPanelCloseBtn", Btn_CloseAlertUP)

    if Fld_UnitsLowBalanceThreshold != "":
        print("Do Something with Fld_UnitsLowBalanceThreshold: ", Fld_UnitsLowBalanceThreshold)
        ElementToBeClickableByID_SendKeys("lowBalanceThresholdBox", Fld_UnitsLowBalanceThreshold)

    if Fld_NotificationEmailAddressesUP != "":
        print("Do Something with Fld_NotificationEmailAddressesUP: ", Fld_NotificationEmailAddressesUP)
        ElementToBeClickableByID_SendKeys("notificationEmailBox", Fld_NotificationEmailAddressesUP)

    if Fld_NotificationPhoneNumbersUP != "":
        print("Do Something with Fld_NotificationPhoneNumbersUP: ", Fld_NotificationPhoneNumbersUP)
        ElementToBeClickableByID_SendKeys("notificationPhoneBox", Fld_NotificationPhoneNumbersUP)

    if Exp_PhysicalAddressUP != "":
        print("Do Something with Exp_PhysicalAddressUP: ", Exp_PhysicalAddressUP)
        ElementToBeClickableByID_Click("usagePointPhysicalAddressPanel-header", Exp_PhysicalAddressUP)

    if Drp_LocationLvl1UP != "":
        print("Do Something with Drp_LocationLvl1UP: ", Drp_LocationLvl1UP)
        ElementToBeClickableByXPATH_DropDown('id("usagePointLocationComponent")/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/table[@class="pseudoCaptionPanel"]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[@class="formRowPanel_panel"]/div[@class="formElement_panel"]/div[@class="formElement_innerPanel"]/div[1]/select[@id="selectionDataBox0"]', Drp_LocationLvl1UP)

    if Drp_LocationLvl2UP != "":
        print("Do Something with Drp_LocationLvl2UP: ", Drp_LocationLvl2UP)
        ElementToBeClickableByID_DropDown("selectionDataBox1", Drp_LocationLvl2UP)

    if Drp_LocationLvl3UP != "":
        print("Do Something with Drp_LocationLvl3UP: ", Drp_LocationLvl3UP)
        ElementToBeClickableByID_DropDown("selectionDataBox2", Drp_LocationLvl3UP)

    if Drp_LocationLvl4UP != "":
        print("Do Something with Drp_LocationLvl4UP: ", Drp_LocationLvl4UP)
        ElementToBeClickableByID_DropDown("selectionDataBox3", Drp_LocationLvl4UP)

    if Fld_ErfNumberUP != "":
        print("Do Something with Fld_ErfNumberUP: ", Fld_ErfNumberUP)
        ElementToBeClickableByID_SendKeys("erfNumberBox", Fld_ErfNumberUP)

    if Fld_StreetNumberUP != "":
        print("Do Something with Fld_StreetNumberUP: ", Fld_StreetNumberUP)
        ElementToBeClickableByID_SendKeys("streetNumberBox", Fld_StreetNumberUP)

    if Fld_BuildingNameUP != "":
        print("Do Something with Fld_BuildingNameUP: ", Fld_BuildingNameUP)
        ElementToBeClickableByID_SendKeys("buildingNameBox", Fld_BuildingNameUP)

    if Fld_SuiteNumberUP != "":
        print("Do Something with Fld_SuiteNumberUP: ", Fld_SuiteNumberUP)
        ElementToBeClickableByID_SendKeys("suiteNumberBox", Fld_SuiteNumberUP)

    if Fld_AddressLine1UP != "":
        print("Do Something with Fld_AddressLine1UP: ", Fld_AddressLine1UP)
        ElementToBeClickableByID_SendKeys("addressBox1", Fld_AddressLine1UP)

    if Fld_AddressLine2UP != "":
        print("Do Something with Fld_AddressLine2UP: ", Fld_AddressLine2UP)
        ElementToBeClickableByID_SendKeys("addressBox2", Fld_AddressLine2UP)

    if Fld_AddressLine3UP != "":
        print("Do Something with Fld_AddressLine3UP: ", Fld_AddressLine3UP)
        ElementToBeClickableByID_SendKeys("addressBox3", Fld_AddressLine3UP)

    if Fld_LatitudeUP != "":
        print("Do Something with Fld_LatitudeUP: ", Fld_LatitudeUP)
        ElementToBeClickableByID_SendKeys("latitudeBox", Fld_LatitudeUP)

    if Fld_LongitudeUP != "":
        print("Do Something with Fld_AddressLine2UP: ", Fld_LongitudeUP)
        ElementToBeClickableByID_SendKeys("longitudeBox", Fld_LongitudeUP)

    if Exp_UsagePointGroups != "":
        print("Do Something with Exp_UsagePointGroups: ", Exp_UsagePointGroups)
        ElementToBeClickableByID_Click("usagePointDisclosurePanel", Exp_UsagePointGroups)

    if Drp_UPG1Lvl1 != "":
        print("Do Something with Drp_UPG1Lvl1: ", Drp_UPG1Lvl1)
        ElementToBeClickableByXPATH_DropDown('/html[1]/body[1]/div[3]/div[2]/div[1]/div[5]/div[@class="gwt-TabLayoutPanel-mainpanel"]/div[3]/div[@class="gwt-TabLayoutPanelContentContainer"]/div[3]/div[@class="gwt-TabLayoutPanelContent"]/div[1]/div[1]/div[3]/table[@id="usagepointcomponent"]/tbody[1]/tr[2]/td[1]/div[1]/div[@class="content"]/div[@class="disclosureContent"]/div[@class="formElementsPanel"]/table[@class="gwt-DisclosurePanel-insidecomponent gwt-DisclosurePanel-insidecomponent-open"]/tbody[1]/tr[2]/td[1]/div[1]/div[@class="content"]/div[1]/div[1]/table[@id="groupTypesPanel"]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/table[@class="pseudoCaptionPanel"]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[@class="formRowPanel_panel"]/div[@class="formElement_panel"]/div[@class="formElement_innerPanel"]/div[1]/select[@id="selectionDataBox0"]', Drp_UPG1Lvl1)

    if Drp_UPG1Lvl2 != "":
        print("Do Something with Drp_UPG1Lvl2: ", Drp_UPG1Lvl2)
        ElementToBeClickableByID_DropDown("selectionDataBox1", Drp_UPG1Lvl2)

    if Drp_UPG1Lvl3 != "":
        print("Do Something with Drp_UPG1Lvl3: ", Drp_UPG1Lvl3)
        ElementToBeClickableByID_DropDown("selectionDataBox2", Drp_UPG1Lvl3)

    if Drp_UPG2Lvl1 != "":
        print("Do Something with Drp_UPG2Lvl1: ", Drp_UPG2Lvl1)
        ElementToBeClickableByID_DropDown("selectionDataBox0", Drp_UPG2Lvl1)

    if Drp_UPG2Lvl2 != "":
        print("Do Something with Drp_UPG2Lvl2: ", Drp_UPG2Lvl2)
        ElementToBeClickableByID_DropDown("selectionDataBox1", Drp_UPG2Lvl2)

    if Drp_UPG2Lvl3 != "":
        print("Do Something with Drp_UPG2Lvl3: ", Drp_UPG2Lvl3)
        ElementToBeClickableByID_DropDown("selectionDataBox2", Drp_UPG2Lvl3)

    if Drp_NonDisconnectLvl1 != "":
        print("Do Something with Drp_NonDisconnectLvl1: ", Drp_NonDisconnectLvl1)
        ElementToBeClickableByID_DropDown("selectionDataBox0", Drp_NonDisconnectLvl1)

    if Btn_SaveUP != "":
        print("Do Something with Btn_SaveUP: ", Btn_SaveUP)
        ElementToBeClickableByID_Click("usagePointSaveButton", Btn_SaveUP)

    if Btn_CancelUP != "":
        print("Do Something with Btn_CancelUP: ", Btn_CancelUP)
        ElementToBeClickableByID_Click("usagePointCancelButton", Btn_CancelUP)

    if Btn_CalcTariff != "":
        print("Do Something with Btn_CalcTariff: ", Btn_CalcTariff)
        ElementToBeClickableByID_Click("btnCalc", Btn_CalcTariff)

    if Btn_DiscardChangesUP != "":
        print("Do Something with Btn_DiscardChangesUP: ", Btn_DiscardChangesUP)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChangesUP)

    if Btn_CancelDiscardChanges != "":
        print("Do Something with Btn_CancelDiscardChanges: ", Btn_CancelDiscardChanges)
        ElementToBeClickableByID_Click("questionPanelCancelButton", Btn_CancelDiscardChanges)

    if Function_UP != "":
        print("Do Something with Function_UP: ", Function_UP)

        if Element_UP != "":
            print("Do Something with Element_UP: ", Element_UP)

            if Assert_UP != "":
                print("Do Something with Assert_UP :", Assert_UP)

                globals()[Function_UP](Element_UP, Assert_UP)


    if Btn_ConfirmDateAndTime != "":
        print("Do Something with Btn_DiscardChangesUP: ", Btn_DiscardChangesUP)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChangesUP)

    if Btn_CancelConfirmDateAndTime != "":
        print("Do Something with Btn_DiscardChangesUP: ", Btn_DiscardChangesUP)
        ElementToBeClickableByID_Click("questionPanelCancelButton", Btn_DiscardChangesUP)

    if Btn_Yes != "":
        print("Do Something with Btn_DiscardChangesUP: ", Btn_DiscardChangesUP)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChangesUP)

    if Btn_No != "":
        print("Do Something with Btn_DiscardChangesUP: ", Btn_DiscardChangesUP)
        ElementToBeClickableByID_Click("questionPanelCancelButton", Btn_DiscardChangesUP)

    if Fld_IntialReading != "":
        print("Do Something with Fld_IntialReading: ", Fld_IntialReading)
        ElementToBeClickableByXPATH_SendKeys("//body[1]/div[5]/div[1]/table[1]/tbody[1]/tr[2]/td[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/table[3]/tbody[1]/tr[1]/td[5]/div[1]/input[1]", Fld_IntialReading)

    if Btn_InitialReadingSave != "":
        print("Do Something with Btn_InitialReadingSave: ", Btn_InitialReadingSave)
        ElementToBeClickableByID_Click("channelReadingsSaveBtn", Btn_InitialReadingSave)


    if Btn_InitialReadingCancel != "":
        print("Do Something with Btn_InitialReadingCancel: ", Btn_InitialReadingCancel)
        ElementToBeClickableByID_Click("channelReadingsCancelBtn", Btn_InitialReadingCancel)


# Fetch Usage Point
    if Lnk_FetchUsagePoint != "":
        print("Do Something with Lnk_FetchUsagePoint: ", Lnk_FetchUsagePoint)
        ElementToBeClickableByID_Click("fetchUsagePointLabel", Lnk_FetchUsagePoint)

    if Fld_EnterUsagePointName != "":
        print("Do Something with Fld_EnterUsagePointName: ", Fld_EnterUsagePointName)
        ElementToBeClickableByID_SendKeys("assignUsagePointBox", Fld_EnterUsagePointName)

    if Btn_FetchUsagePoint != "":
        print("Do Something with Btn_FetchUsagePoint: ", Btn_FetchUsagePoint)
        ElementToBeClickableByID_Click("assignUsagePointButton", Btn_FetchUsagePoint)

    if Btn_cancelFindUsagePoint != "":
        print("Do Something with Btn_cancelFindUsagePoint: ", Btn_cancelFindUsagePoint)
        ElementToBeClickableByID_Click("btnCancel", Btn_cancelFindUsagePoint)

    if Btn_DiscardChangesFindUsagePoint != "":
        print("Do Something with Btn_DiscardChangesFindUsagePoint: ", Btn_DiscardChangesFindUsagePoint)
        ElementToBeClickableByID_Click("questionPanelConfirmButton", Btn_DiscardChangesFindUsagePoint)

    if Btn_CancelDiscardChangesFindUsagePoint != "":
        print("Do Something with Btn_CancelDiscardChangesFindUsagePoint: ", Btn_CancelDiscardChangesFindUsagePoint)
        ElementToBeClickableByID_Click("questionPanelCancelButton", Btn_CancelDiscardChangesFindUsagePoint)


print("-------- MMA Add Contract EXECUTION START --------")
#Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = (sys.argv)[0]
y = int(PreID)
print("PREID TO USE: ",PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n","---- TEST DATA ROW INDEX: ",rowIndex," ----","\n")
        ReadData(rowIndex)
        print("\n","---- FRAMEWORK ROW INDEX: ",rowIndex," ----","\n")
        Framework()

print("-------- MMA Add Contract EXECUTION COMPLETE --------")

