import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("UsagePointHist-PhysicalAddrHist")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_UsagePointInformation
    colIndex += 1
    Exp_UsagePointInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointInformation: ", Exp_UsagePointInformation)

    global Exp_UsagePointHistory
    colIndex += 1
    Exp_UsagePointHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_UsagePointHistory: ", Exp_UsagePointHistory)

    global SortOldToRecent
    colIndex += 1
    SortOldToRecent = sheet.row_values(rowIndex)[colIndex]
    print("SortOldToRecent: ", SortOldToRecent)

    global DateModified_col
    colIndex += 1
    DateModified_col = sheet.row_values(rowIndex)[colIndex]
    print("DateModified_col: ", DateModified_col)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global ByUser_col
    colIndex += 1
    ByUser_col = sheet.row_values(rowIndex)[colIndex]
    print("ByUser_col: ", ByUser_col)

    global ByUser
    colIndex += 1
    ByUser = sheet.row_values(rowIndex)[colIndex]
    print("ByUser: ", ByUser)

    global Action_col
    colIndex += 1
    Action_col = sheet.row_values(rowIndex)[colIndex]
    print("Action_col: ", Action_col)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global Status_col
    colIndex += 1
    Status_col = sheet.row_values(rowIndex)[colIndex]
    print("Status_col: ", Status_col)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global Address1_col
    colIndex += 1
    Address1_col = sheet.row_values(rowIndex)[colIndex]
    print("Address1_col: ", Address1_col)

    global Address1
    colIndex += 1
    Address1 = sheet.row_values(rowIndex)[colIndex]
    print("Address1: ", Address1)

    global Address2_col
    colIndex += 1
    Address2_col = sheet.row_values(rowIndex)[colIndex]
    print("Address2_col: ", Address2_col)

    global Address2
    colIndex += 1
    Address2 = sheet.row_values(rowIndex)[colIndex]
    print("Address2: ", Address2)

    global Address3_col
    colIndex += 1
    Address3_col = sheet.row_values(rowIndex)[colIndex]
    print("Address3_col: ", Address3_col)

    global Address3
    colIndex += 1
    Address3 = sheet.row_values(rowIndex)[colIndex]
    print("Address3: ", Address3)

    global ErfNum_col
    colIndex += 1
    ErfNum_col = sheet.row_values(rowIndex)[colIndex]
    print("ErfNum_col: ", ErfNum_col)

    global ErfNum
    colIndex += 1
    ErfNum = sheet.row_values(rowIndex)[colIndex]
    print("ErfNum: ", ErfNum)

    global Latitude_col
    colIndex += 1
    Latitude_col = sheet.row_values(rowIndex)[colIndex]
    print("Latitude_col: ", Latitude_col)

    global Latitude
    colIndex += 1
    Latitude = sheet.row_values(rowIndex)[colIndex]
    print("Latitude: ", Latitude)

    global Longitude_col
    colIndex += 1
    Longitude_col = sheet.row_values(rowIndex)[colIndex]
    print("Longitude_col: ", Longitude_col)

    global Longitude
    colIndex += 1
    Longitude = sheet.row_values(rowIndex)[colIndex]
    print("Longitude: ", Longitude)

    global Group_col
    colIndex += 1
    Group_col = sheet.row_values(rowIndex)[colIndex]
    print("Group_col: ", Group_col)

    global Group
    colIndex += 1
    Group = sheet.row_values(rowIndex)[colIndex]
    print("Group: ", Group)

    global StreetNum_col
    colIndex += 1
    StreetNum_col = sheet.row_values(rowIndex)[colIndex]
    print("StreetNum_col: ", StreetNum_col)

    global StreetNum
    colIndex += 1
    StreetNum = sheet.row_values(rowIndex)[colIndex]
    print("StreetNum: ", StreetNum)

    global Building_col
    colIndex += 1
    Building_col = sheet.row_values(rowIndex)[colIndex]
    print("Building_col: ", Building_col)

    global Building
    colIndex += 1
    Building = sheet.row_values(rowIndex)[colIndex]
    print("Building: ", Building)

    global SuiteNum_col
    colIndex += 1
    SuiteNum_col = sheet.row_values(rowIndex)[colIndex]
    print("SuiteNum_col: ", SuiteNum_col)

    global SuiteNum
    colIndex += 1
    SuiteNum = sheet.row_values(rowIndex)[colIndex]
    print("SuiteNum: ", SuiteNum)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)


def Framework():
    #variables
    UsagePointInfoDiv = 3  # Usage Point information div
    UsagePointHistoryRow = 2 # Usage Point history row within Usage Point information
    UPPhysicalAddrHistTableRow = 2 # Usage Point physical Address history table row within Usage Point history row
    Tr = 4
    MoreXpath = ""

    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Exp_UsagePointInformation != "":
        print("Do Something with Exp_UsagePointInformation: ", Exp_UsagePointInformation)
        ElementToBeClickableByXPATH_Click("//div[contains(text(),'Usage Point Information')]", Exp_UsagePointInformation)

    if Exp_UsagePointHistory != "":
        print("Do Something with Exp_UsagePointHistory: ", Exp_UsagePointHistory)
        ElementToBeClickableByXPATH_Click("//td[contains(text(),'Usage Point History')]", Exp_UsagePointHistory)

    if SortOldToRecent != "":
        print("Do Something with SortOldToRecent: ", SortOldToRecent)
        ElementToBeClickableByXPATH_Click("//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/thead[1]/tr[1]/th[1]/div[1]/div[2]", SortOldToRecent)

    if DateModified != "":
        print("Do Something with DateModified: ", DateModified)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, DateModified_col, DateModified)

    if ByUser != "":
        print("Do Something with ByUser: ", ByUser)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, ByUser_col, ByUser)

    if Action != "":
        print("Do Something with Action: ", Action)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Action_col, Action)

    if Status != "":
        print("Do Something with Status: ", Status)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Status_col, Status)

    if Address1 != "":
        print("Do Something with Address1: ", Address1)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Address1_col, Address1)

    if Address2 != "":
        print("Do Something with Address2: ", Address2)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Address2_col, Address2)

    if Address3 != "":
        print("Do Something with Address3: ", Address3)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Address3_col, Address3)

    if ErfNum != "":
        print("Do Something with ErfNum: ", ErfNum)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, ErfNum_col, ErfNum)

    if Latitude != "":
        print("Do Something with Latitude: ", Latitude)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Latitude_col, Latitude)

    if Longitude != "":
        print("Do Something with Longitude: ", Longitude)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Longitude_col, Longitude)

    if Group != "":
        print("Do Something with Group: ", Group)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Group_col, Group)

    if StreetNum != "":
        print("Do Something with StreetNum: ", StreetNum)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, StreetNum_col, StreetNum)

    if Building != "":
        print("Do Something with Building: ", Building)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, Building_col, Building)

    if SuiteNum != "":
        print("Do Something with SuiteNum: ", SuiteNum)
        WebTableTextToBePresentInElementByXPATH(UsagePointInfoDiv, UsagePointHistoryRow, UPPhysicalAddrHistTableRow, Tr, MoreXpath, RowNumber, SuiteNum_col, SuiteNum)

    if NextPage != "":
        print("Do Something with NextPage: ", NextPage)
        ElementToBeClickableByXPATH_Click(
            "//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[5]/td[1]/table[1]/tbody[1]/tr[1]/td[4]/img[1]",
            NextPage)


print("-------- MMA USAGE POINT HISTORY-USAGE POINT PHYSICAL ADDRESS HISTORY EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()

print("-------- MMA USAGE POINT HISTORY-USAGE POINT PHYSICAL ADDRESS HISTORY EXECUTION COMPLETE --------")
