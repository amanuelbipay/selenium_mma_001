# import sys


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


"""
the webdriver will wait for a page to load by default. It does not wait for loading inside frames or for ajax requests. 
It means when you use .get('url'), your browser will wait until the page is completely loaded and then go to the next 
command in the code. But when you are posting an ajax request, webdriver does not wait and it's your responsibility to 
wait an appropriate amount of time for the page or a part of page to load; so there is a module named expected_conditions.
"""
"""
desired_capabilities = DesiredCapabilities.CHROME.copy()
# desired_capabilities = DesiredCapabilities.FIREFOX.copy()
desired_capabilities['acceptInsecureCerts'] = True

# browser = webdriver.Firefox(executable_path=r'C:\Temp\geckodriver.exe',capabilities=desired_capabilities)
global browser
browser = webdriver.Chrome(executable_path=r'C:\Temp\chromedriver.exe', desired_capabilities=desired_capabilities)
browser.get('https://test-server-qa3:9443/MeterMngAdmin/login.do')
"""
"""
#OLD METHOD
browser = webdriver.Firefox(executable_path=r'C:\Temp\geckodriver.exe')
browser.get('https://test-server-qa2:9443/MeterMngAdmin/login.do')
"""
# assert 'MeterMngAdmin' in browser.title

import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("Login")


def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Fld_User
    colIndex += 1
    Fld_User = sheet.row_values(rowIndex)[colIndex]
    print("Fld_User: ", Fld_User)

    global Fld_Password
    colIndex += 1
    Fld_Password = sheet.row_values(rowIndex)[colIndex]
    print("Fld_Password: ", Fld_Password)

    global Btn_Login
    colIndex += 1
    Btn_Login = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Login: ", Btn_Login)

    global Btn_Clear
    colIndex += 1
    Btn_Clear = sheet.row_values(rowIndex)[colIndex]
    print("Btn_Clear: ", Btn_Clear)

    global Lnk_ForgottenPassword
    colIndex += 1
    Lnk_ForgottenPassword = sheet.row_values(rowIndex)[colIndex]
    print("Lnk_ForgottenPassword: ", Lnk_ForgottenPassword)

    global Function
    colIndex += 1
    Function = sheet.row_values(rowIndex)[colIndex]
    print("Function: ", Function)

    global Element
    colIndex += 1
    Element = sheet.row_values(rowIndex)[colIndex]
    print("Element: ", Element)

    global Assert
    colIndex += 1
    Assert = sheet.row_values(rowIndex)[colIndex]
    print("Assert: ", Assert)


def Framework():
    if Exe == "P":
        print("Do Something with Exe")
    else:
        print("Exit loop Exe != P")
        return

    if Fld_User != "":
        print("Do Something with User: ", Fld_User)
        ElementToBeClickableByNAME_SendKeys("j_username", Fld_User)

        """
        wait = WebDriverWait(browser, 10)
        FldUser = wait.until(EC.element_to_be_clickable((By.NAME,"j_username")))
        FldUser.send_keys(User)
        #FldUser = browser.find_element(By.NAME,"j_username")
        #FldUser.send_keys(User)
        """

    if Fld_Password != "":
        print("Do Something with Password :", Fld_Password)
        ElementToBeClickableByNAME_SendKeys("j_password", Fld_Password)
        # FldPassword = browser.find_element(By.NAME,"j_password")
        # FldPassword.send_keys(Password)

    if Btn_Login != "":
        print("Do Something with Login Button: ", Btn_Login)
        ElementToBeClickableByNAME_Click("submit", Btn_Login)

    if Btn_Clear != "":
        print("Do Something with Clear Button: ", Btn_Clear)
        ElementToBeClickableByNAME_Click("clear", Btn_Clear)

    if Function != "":
        print("Do Something with Function: ", Function)

        if Element != "":
            print("Do Something with Element: ", Element)

            if Assert != "":
                print("Do Something with Assert :", Assert)

                globals()[Function](Element, Assert)









print("-------- MMA LOGIN EXECUTION START --------")
# Get PreID RowIndex Number to use to speed up lookup for ReadData function.
PreID = sys.argv[0]
y = int(PreID)
print("PREID TO USE: ", PreID)
for i, cell in enumerate(sheet.col(2)):
    if cell.value == y:
        rowIndex = i
        print("\n", "---- TEST DATA ROW INDEX: ", rowIndex, " ----", "\n")
        ReadData(rowIndex)
        print("\n", "---- FRAMEWORK ROW INDEX: ", rowIndex, " ----", "\n")
        Framework()
# browser.close()
# workbook.close()#CLOSE TEST RESULTS WORKBOOK
print("-------- MMA LOGIN EXECUTION COMPLETE --------")
