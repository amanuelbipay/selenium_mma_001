from time import sleep
from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException


desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())
#browser = webdriver.Chrome(ChromeDriverManager().install(), desired_capabilities = desired_capabilities)
browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get("https://www.tutorialspoint.com/index.htm")




#identify element
l= browser.find_elements_by_css_selector("h41")
#get list size with len
s = len(l)
# check condition, if list size > 0, element exists
if(s>0):
   #m= l.text
   print("Element exist")
else:
   print("Element does not exist")
browser.close()

