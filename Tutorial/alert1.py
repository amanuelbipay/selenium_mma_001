from time import sleep
from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException


desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())
#browser = webdriver.Chrome(ChromeDriverManager().install(), desired_capabilities = desired_capabilities)
browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get("https://www.python.org/")


def alert():
    create_alert = "alert('There was something wrong. Check the log');"
    browser.execute_script(create_alert)
    alert = browser.switch_to.alert
    alertText = browser.switch_to.alert.text
    print("Alert text: ", alertText)

    global alertvisible
    alertvisible = True
    # sleep(5)

    try:
        wait = WebDriverWait(browser, timeout=2)
        wait.until(EC.alert_is_present())
        # x = "True"

        alertvisible = True
        print(alertvisible)

    except:
        # y = "False"
        alertvisible = False
        print(alertvisible)
        # browser.quit()

    while alertvisible == True:
        print("do  nothing")
        isAlertPresent()
        time.sleep(1)
    print("to continue")
    browser.quit()










alert()
