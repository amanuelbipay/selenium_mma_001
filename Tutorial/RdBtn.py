from time import sleep
from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException



desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())

browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get("https://fs2.formsite.com/meherpavan/form2/index")
#element = browser.find_element(By.XPATH, "(//label[normalize-space()='Female'])[1]")
#element.click()



def ElementToBeClickableByIDX_Check(Element):
    try:
        wait = WebDriverWait(browser, 10)
        element = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
        element = browser.find_element(By.XPATH, Element)
        element.click()
        element = browser.find_element(By.XPATH, Element).is_selected()
        print("ElementToBeClickableByID_Check :", Element, " - PASSED")
    except:
        print("ElementToBeClickableByID_Check :", Element, " - FAILED")


ElementToBeClickableByIDX_Check("(//label[normalize-space()='Female'])[1]")

