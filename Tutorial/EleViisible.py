from time import sleep
from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException


desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())
#browser = webdriver.Chrome(ChromeDriverManager().install(), desired_capabilities = desired_capabilities)
browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get("https://www.python.org/")

Data = "Visible"
Element = "homepage"

def isElementPresent(Element, Data, ElemLocator):
    if Data == "Visible" or "Invisible" :
        try:
            wait = WebDriverWait(browser, timeout=2)
            wait.until(EC.find_element((ElemLocator, Element)))
            elementVisible = "ElementToBePresentByID :", Element, " -is Visible"
            print(elementVisible)
        except:
            elementVisible = "ElementToBePresentByID :", Element, " -is InVisible"
            print(elementVisible)


isElementPresent("homepage", "Visible", By.ID)


