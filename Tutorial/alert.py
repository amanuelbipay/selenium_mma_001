from time import sleep
from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException


desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())
#browser = webdriver.Chrome(ChromeDriverManager().install(), desired_capabilities = desired_capabilities)
browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get("https://www.python.org/")

"""
# create a simple alert with text "Custom Alert"
create_alert = "alert('Custom Alert');"
browser.execute_script(create_alert)

sleep(2)

# switch to alert
alert = browser.switch_to.alert

# try to send keys
try:
    alert.send_keys("Python")
except ElementNotInteractableException as e:
    print(f"Received ElementNotInteractableException with message: {e.args[0]}")

    # close alert and give control to the parent
    #alert.accept()

"""







"""
# create a confirmation alert
create_confirmation_alert = "confirm('Confirmation Alert Box');"
browser.execute_script(create_confirmation_alert)

sleep(4)

# switch to alert
alert = browser.switch_to.alert

# dismiss/cancel the alert
alert.dismiss()

sleep(2)
"""





"""

# create a prompt alert
create_prompt_alert = "prompt('What is your favourite programming language?');"
create_prompt_alert2 = "prompt('There was a problem');"
browser.execute_script(create_prompt_alert)
browser.execute_script(create_prompt_alert2)
# switch to alert
alert = browser.switch_to.alert

sleep(3)

wait = WebDriverWait(browser, timeout=15)
wait.until(EC.alert_is_present())

# fill the text input field
#alert.send_keys("Python")


#sleep(2)

#alert.accept()
"""

def alert():
    create_alert = "alert('There was something wrong');"
    browser.execute_script(create_alert)
    alert = browser.switch_to.alert
    alertText = browser.switch_to.alert.text
    print("Alert text: ", alertText)


def isAlertPresent():
    global alertvisible
    alertvisible = True
    # sleep(5)

    try:
        wait = WebDriverWait(browser, timeout=2)
        wait.until(EC.alert_is_present())
        #x = "True"

        alertvisible= True
        print(alertvisible)

    except:
        #y = "False"
        alertvisible = False
        print(alertvisible)
        #browser.quit()


def holdAlert():
    #for x in range(20):
    while alertvisible == True:
        print("do  nothing")
        isAlertPresent()
        time.sleep(1)
    print("to continue")
    browser.quit()



alert()
isAlertPresent()
holdAlert()












