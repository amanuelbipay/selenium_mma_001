
from selenium import webdriver

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.chrome.service import Service



desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())
browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get("https://www.youtube.com/")
browser.maximize_window()
browser.execute_script("window.scrollBy(0,100)")

