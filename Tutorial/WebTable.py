from time import sleep
from selenium import webdriver
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException


desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())
#browser = webdriver.Chrome(ChromeDriverManager().install(), desired_capabilities = desired_capabilities)
browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get("https://qavbox.github.io/demo/webtable/")

table = browser.find_element(by=By.ID, value="table01")
header = table.find_elements(by=By.TAG_NAME, value="th")
rows = table.find_elements(by=By.TAG_NAME, value="tr")
body = table.find_element(by=By.TAG_NAME, value="tbody")
cells = body.find_elements(by=By.TAG_NAME, value="td")

print("-------- Number of rows--------")
rowNumber = len(rows)
print("Number of rows: ", rowNumber)

"""
print("-------- Locating a cell using for loop -  START--------")

for i in range(rowNumber):
    columns = rows[i].find_elements(by=By.TAG_NAME, value="td")
    for j in range(len(columns)):
        if columns[j].text == "TFS":
            ManualTesting = columns[1].text
            AutomationTesting = columns[2].text
            IssueTracker = columns[3].text
            print(ManualTesting, AutomationTesting, IssueTracker)
            
print("-------- Headers - START --------")
for headerEL in header:
    print(headerEL.text)
print("-------- Headers - END --------")


print("-------- Cells - START--------")
for cell in cells:
    print(cell.text)
print("-------- Cells - END--------")


print("-------- Locating a cell using for loop -  END--------")
"""

print("-------- Locating a cell using XPATH -  START--------")

ManualTesting = browser.find_element(by=By.XPATH, value="/html[1]/body[1]/form[1]/fieldset[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]").text
AutomationTesting = browser.find_element(by=By.XPATH, value="//td[normalize-space()='Bugzilla']").text
IssueTracker = browser.find_element(by=By.XPATH, value="//table['@id=table01']/tbody/tr[2]/td[4]").text
print(ManualTesting, AutomationTesting, IssueTracker)

print("-------- Locating a cell using XPATH -  END--------")

#working on the webtable
#another developer did this

#browser.quit()

   