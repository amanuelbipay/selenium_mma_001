def WebTableTextToBePresentInElementByXPATH(Div, InfoRow, TabelRow, Tr, CellRowIndex, CellColIndex, Data):
    #//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1] another method
    #//div[div of MeterInfo]/table[1]/tbody[1]/tr[Row of MeterHistory]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[Row of MeterHistoryTable]/td[1]/div[1]/table[1]/tbody[1]/tr[Tr]/td[1]/table[1]/tbody[1]/tr[CellRowNumber]/td[CellColNumber]/div[1]
    Element1 = "//div["
    Element2 = "]/table[1]/tbody[1]/tr["
    Element3 = "]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element4 = "]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element5 = "]/td[1]/table[1]/tbody[1]/tr["
    Element6 = "]/td["
    Element7 = "]/div[1]"
    Element = Element1+str(Div)+Element2+str(InfoRow)+Element3+str(TabelRow)+Element4+str(Tr)+Element5+str(CellRowIndex)+Element6+str(CellColIndex)+Element7

    wait = WebDriverWait(browser, 10)
    text = browser.find_element(by=By.XPATH, value=Element).text
    print("The text present in the element is: ", text)
    ASSERT = Data
    if ASSERT == text:
        print("WebTableTextToBePresentInElementByXPATH :", Element, " - PASSED")

    else:
        print("WebTableTextToBePresentInElementByXPATH :", Element, " - FAILED")
        holdAlertText = "WebTableTextToBePresentInElementByXPATH :", Element, " - FAILED"
        # Alert when something is wrong - START
        alert()
        isAlertPresent()
        holdAlert(holdAlertText)
        # Alert when something is wrong - END



def WebTableElementToBeClickableByXPATH_Click(Div, InfoRow, TabelRow, Tr, CellRowIndex, CellColIndex, Data):
    Element1 = "//div["
    Element2 = "]/table[1]/tbody[1]/tr["
    Element3 = "]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element4 = "]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element5 = "]/td[1]/table[1]/tbody[1]/tr["
    Element6 = "]/td["
    Element7 = "]/div[1]"
    Element = Element1 + str(Div) + Element2 + str(InfoRow) + Element3 + str(TabelRow) + Element4 + str(
        Tr) + Element5 + str(CellRowIndex) + Element6 + str(CellColIndex) + Element7

    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
            element.click()
            print("ElementToBeClickableByXPATH_Click :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByXPATH_Click :", Element, " - FAILED")