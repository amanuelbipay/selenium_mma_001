import xlrd

InputData = xlrd.open_workbook(r"C:\Users\User\PycharmProjects\Framework_1\Data\MMA.xlsx")
sheet = InputData.sheet_by_name("MeterInformation")

def ReadData(rowIndex):
    colIndex = -1

    global Exe
    colIndex += 1
    Exe = sheet.row_values(rowIndex)[colIndex]
    print("Exe: ", Exe)

    global ID
    colIndex += 1
    ID = sheet.row_values(rowIndex)[1]
    print("ID: ", ID)

    global PreID
    colIndex += 1
    PreID = sheet.row_values(rowIndex)[2]
    print("PreID: ", PreID)

    global PreReqStep
    colIndex += 1
    PreReqStep = sheet.row_values(rowIndex)[colIndex]
    print("PreReqStep: ", PreReqStep)

    global Description
    colIndex += 1
    Description = sheet.row_values(rowIndex)[colIndex]
    print("Description: ", Description)

    global Exp_MeterInformation
    colIndex += 1
    Exp_MeterInformation = sheet.row_values(rowIndex)[colIndex]
    print("Exp_MeterInformation: ", Exp_MeterInformation)

    global Exp_MeterHistory
    colIndex += 1
    Exp_MeterHistory = sheet.row_values(rowIndex)[colIndex]
    print("Exp_MeterHistory: ", Exp_MeterHistory)

    global SortRecentToOld
    colIndex += 1
    SortRecentToOld = sheet.row_values(rowIndex)[colIndex]
    print("SortRecentToOld: ", SortRecentToOld)

    global DateModified
    colIndex += 1
    DateModified = sheet.row_values(rowIndex)[colIndex]
    print("DateModified: ", DateModified)

    global ByUser
    colIndex += 1
    ByUser = sheet.row_values(rowIndex)[colIndex]
    print("ByUser: ", ByUser)

    global Action
    colIndex += 1
    Action = sheet.row_values(rowIndex)[colIndex]
    print("Action: ", Action)

    global Status
    colIndex += 1
    Status = sheet.row_values(rowIndex)[colIndex]
    print("Status: ", Status)

    global Serial
    colIndex += 1
    Serial = sheet.row_values(rowIndex)[colIndex]
    print("Serial: ", Serial)

    global UniqueID
    colIndex += 1
    UniqueID = sheet.row_values(rowIndex)[colIndex]
    print("UniqueID: ", UniqueID)

    global ExtrenalUniqueID
    colIndex += 1
    ExtrenalUniqueID = sheet.row_values(rowIndex)[colIndex]
    print("ExtrenalUniqueID: ", ExtrenalUniqueID)

    global Store
    colIndex += 1
    Store = sheet.row_values(rowIndex)[colIndex]
    print("Store: ", Store)

    global RowNumber
    colIndex += 1
    RowNumber = sheet.row_values(rowIndex)[colIndex]
    print("RowNumber: ", RowNumber)

    global ActionRowNum
    colIndex += 1
    ActionRowNum = sheet.row_values(rowIndex)[colIndex]
    print("ActionRowNum: ", ActionRowNum)

    global NextPage
    colIndex += 1
    NextPage = sheet.row_values(rowIndex)[colIndex]
    print("NextPage: ", NextPage)

def ReadCOlData(colIndex):
    rowNumIndex = 1

    global ColmunNumber
    ColmunNumber = sheet.row_values(rowNumIndex)[colIndex]
    print("Col num: ", ColmunNumber)


def WebTableRCTextToBePresentInElementByXPATH(Div, InfoRow, TabelRow,CellRowIndex, CellColIndex):
        Element1 = "//div["
        Element2 = "]/table[1]/tbody[1]/tr["
        Element3 = "]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr["
        Element4 = "]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr["
        Element5 = "]/td["
        Element6 = "]/div[1]"
        Element = Element1 + str(Div) + Element2 + str(InfoRow) + Element3 + str(TabelRow) + Element4 + str(
            CellRowIndex) + Element5 + str(CellColIndex) + Element6
        print(Element)



for i, cell in enumerate(sheet.row(1)):
    ReadCOlData(i)

for i, cell in enumerate(sheet.col(1)):
    ReadData(i)


#change
WebTableRCTextToBePresentInElementByXPATH(1, 3, 1, 1, int(1))