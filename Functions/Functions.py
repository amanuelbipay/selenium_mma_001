from xmlrpc.client import Boolean

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
import time
from selenium.webdriver.chrome.service import Service


desired_capabilities = DesiredCapabilities().CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True

s=Service(ChromeDriverManager().install())
#browser = webdriver.Chrome(ChromeDriverManager().install(), desired_capabilities = desired_capabilities)
browser = webdriver.Chrome(service=s, desired_capabilities = desired_capabilities)
browser.implicitly_wait(10)
browser.get('https://postgres-abauto--3-17-1.172.23.140.150.nip.io/MeterMngAdmin/login.do')



# Alert when something is wrong - START
def alert():
    create_alert ="alert('Something is wrong check the log before clicking *Ok*');"
    browser.execute_script(create_alert)
    alert = browser.switch_to.alert

def isAlertPresent():
    global alertvisible
    alertvisible = True
    try:
        wait = WebDriverWait(browser, timeout=10)
        wait.until(EC.alert_is_present())
        alertvisible = True
        print(alertvisible)
    except:
        alertvisible = False
        print(alertvisible)

def holdAlert(holdAlertText):
    while alertvisible == True:
        print(holdAlertText)
        isAlertPresent()
        time.sleep(3)
    print("to continue")
# Alert when something is wrong - END


#Check if Element is visible - START
def ElementIsPresent(Element, Data, ElemLocator):
    if Data == "Visible":
        try:
            wait = WebDriverWait(browser, timeout=10)
            wait.until(EC.find_element((ElemLocator, Element)))
            print("Pass - Element," + Element + " is Visible")
        except:
            print("ElementIsNotPresentBy" + ElemLocator + ":", Element, " - FAILED")
#Check if Element is visible  - END


#Check if Element is Invisible - START
def ElementIsNotPresent(Element, Data, ElemLocator):
    if Data == "Invisible":
        try:
            Elem = browser.find_elements(ElemLocator, Element)
            ElemLen = len(Elem)
            if ElemLen > 0:
                print("Fail - Element,"+ Element+" is visible, while expected to be invisible")
            else:
                print("Pass - Element,"+ Element+" is Invisible")
        except:
            print("ElementIsNotPresentBy"+ ElemLocator+":",  Element, " - FAILED")
#Check if Element is Invisible  - END


#SendKeys
def ElementToBeClickableByNAME_SendKeys(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.NAME)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.NAME)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.NAME, Element)))
            element.clear()  # Clear all text before typing
            element.send_keys(Data)
            print("ElementToBeClickableByNAME :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByNAME :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByNAME :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


def ElementToBeClickableByID_SendKeys(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.NAME)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.NAME)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.ID, Element)))
            element.clear()  # Clear all text before typing
            element.send_keys(Data)
            print("ElementToBeClickableByID :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByID :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByID :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


def ElementToBeClickableByXPATH_SendKeys(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
            element.clear()  # Clear all text before typing
            element.send_keys(Data)
            print("ElementToBeClickableByXPATH :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByXPATH :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByXPATH :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


#validation
def TextToBePresentInElementByID(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.ID)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.ID)
    else:
        wait = WebDriverWait(browser, 10)
        text = browser.find_element(by=By.ID, value=Element).text
        print("The text present in the element is: ", text)
        ASSERT = Data
        if ASSERT in text:  # i had to sue In instead of == because confirmations messages get long
            print("TextToBePresentInElementByID :", Element, " - PASSED")
        else:
            print("TextToBePresentInElementByID :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "TextToBePresentInElementByID :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END



def TextToBePresentInElementByXPATH(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        wait = WebDriverWait(browser, 10)
        text = browser.find_element(by=By.XPATH, value=Element).text
        print("The text present in the element is: ", text)
        ASSERT = Data
        if ASSERT in text:
            print("TextToBePresentInElementByXPATH :", Element, " - PASSED")
        else:
            print("TextToBePresentInElementByXPATH :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "TextToBePresentInElementByXPATH :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END

#webtable Validation
def WebTableTextToBePresentInElementByXPATH(Div, InfoRow, TabelRow, Tr, MoreXpath, CellRowIndex, CellColIndex, Data):
    #//div[3]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1] another method
    #//div[div of MeterInfo]/table[1]/tbody[1]/tr[Row of MeterHistory]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[Row of MeterHistoryTable]/td[1]/div[1]/table[1]/tbody[1]/tr[Tr]/td[1]/table[1]/tbody[1]/tr[CellRowNumber]/td[CellColNumber]/div[1]
    # More xpath = div[1]/table[1]/tbody[1]/tr[5]/td[1]/
    # //div[div of MeterInfo]/table[1]/tbody[1]/tr[Row of MeterHistory]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr[Row of MeterHistoryTable]/td[1]/div[1]/table[1]/tbody[1]/tr[Tr]/td[1]/[More xpath]table[1]/tbody[1]/tr[CellRowNumber]/td[CellColNumber]/div[1]
    Element1 = "//div["
    Element2 = "]/table[1]/tbody[1]/tr["
    Element3 = "]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element4 = "]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element5 = "]/td[1]/"
    Element6 = "table[1]/tbody[1]/tr["
    Element7 = "]/td["
    Element8 = "]/div[1]"
    Element = Element1 + str(Div) + Element2 + str(InfoRow) + Element3 + str(TabelRow) + Element4 + str(
        Tr) + Element5 + MoreXpath + Element6 + str(CellRowIndex) + Element7 + str(CellColIndex) + Element8

    wait = WebDriverWait(browser, 10)
    text = browser.find_element(by=By.XPATH, value=Element).text
    print("The text present in the element is: ", text)
    ASSERT = Data
    if ASSERT == text:
        print("WebTableTextToBePresentInElementByXPATH :", Element, " - PASSED")

    else:
        print("WebTableTextToBePresentInElementByXPATH :", Element, " - FAILED")
        # Alert when something is wrong - START
        holdAlertText = "WebTableTextToBePresentInElementByXPATH :", Element, " - FAILED"
        alert()
        isAlertPresent()
        holdAlert(holdAlertText)
        # Alert when something is wrong - END


# Click,link
def ElementToBeClickableByID_executeClick(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.ID)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.ID)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.ID, Element)))
            browser.execute_script("arguments[0].click();",element)
            # https://stackoverflow.com/questions/37879010/selenium-debugging-element-is-not-clickable-at-point-x-y
            print("ElementToBeClickableByID_executeClick :", Element, " - PASSED")
        except Exception as e:
            print("ElementToBeClickableByID_executeClick :", Element, " - FAILED")
            print(e)
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByID_executeClick :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


def ElementToBeClickableByID_Click(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.ID)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.ID)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.ID, Element)))
            element.click()
            print("ElementToBeClickableByID_Click :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByID_Click :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByID_Click :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END



def ElementToBeClickableByXPATH_Click(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
            element.click()
            print("ElementToBeClickableByXPATH_Click :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByXPATH_Click :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByXPATH_Click :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


def ElementToBeClickableByNAME_Click(Element,Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.NAME)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.NAME)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.NAME, Element)))
            element.click()
            print("ElementToBeClickableByNAME_Click :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByNAME_Click :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByNAME_Click :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


def WebTableElementToBeClickableByXPATH_Click(Div, InfoRow, TabelRow, Tr, MoreXpath, CellRowIndex, CellColIndex, Data):
    Element1 = "//div["
    Element2 = "]/table[1]/tbody[1]/tr["
    Element3 = "]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element4 = "]/td[1]/div[1]/table[1]/tbody[1]/tr["
    Element5 = "]/td[1]/"
    Element6 = "table[1]/tbody[1]/tr["
    Element7 = "]/td["
    Element8 = "]/div[1]"
    Element = Element1 + str(Div) + Element2 + str(InfoRow) + Element3 + str(TabelRow) + Element4 + str(
        Tr) + Element5 + MoreXpath + Element6 + str(CellRowIndex) + Element7 + str(CellColIndex) + Element8

    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data == "Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
            element.click()
            print("ElementToBeClickableByXPATH_Click :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByXPATH_Click :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByXPATH_Click :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END

# Drop-down list
def ElementToBeClickableByID_DropDown(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.ID)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.ID)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            select = wait.until(EC.element_to_be_clickable((By.ID, Element)))
            select = Select(browser.find_element(By.ID, Element))
            select.select_by_visible_text(Data)
            print("ElementToBeClickableByID_DropDown :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByID_DropDown :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByID_DropDown :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


def ElementToBeClickableByXPATH_DropDown(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            select = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
            select = Select(browser.find_element(By.ID, Element))
            select.select_by_visible_text(Data)
            print("ElementToBeClickableByXPATH_DropDown :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByXPATH_DropDown :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByXPATH_DropDown :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


# Radio buttons, Check box
def ElementToBeClickableByID_Check(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.ID)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.ID)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.ID, Element)))
            element.click()
            element = browser.find_element(By.ID, Element).is_selected()
            print("ElementToBeClickableByID_Check :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByID_Check :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByID_Check :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END


def ElementToBeClickableByXPATH_Check(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
            element.click()
            element = browser.find_element(By.XPATH, Element).is_selected()
            print("ElementToBeClickableByXPATH_Check :", Element, " - PASSED")
        except:
            print("ElementToBeClickableByXPATH_Check :", Element, " - FAILED")
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByXPATH_Check :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END

def ElementToBeClickableByXPATH_executeCheck(Element, Data):
    if Data == "Visible":
        ElementIsPresent(Element, Data, By.XPATH)
    elif Data =="Invisible":
        ElementIsNotPresent(Element, Data, By.XPATH)
    else:
        try:
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, Element)))
            browser.execute_script("arguments[0].click();",element)
            # https://stackoverflow.com/questions/37879010/selenium-debugging-element-is-not-clickable-at-point-x-y
            print("ElementToBeClickableByXPATH_executeCheck :", Element, " - PASSED")
        except Exception as e:
            print("ElementToBeClickableByXPATH_executeCheck :", Element, " - FAILED")
            print(e)
            # Alert when something is wrong - START
            holdAlertText = "ElementToBeClickableByXPATH_executeCheck :", Element, " - FAILED"
            alert()
            isAlertPresent()
            holdAlert(holdAlertText)
            # Alert when something is wrong - END
